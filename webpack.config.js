const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');
const parts = require('./libs/parts');

const PATHS = {
	app: path.join(__dirname, 'src', 'js', 'client.js'),
	js: path.join(__dirname, 'src', 'js'),
	img: path.join(__dirname, 'src', 'img'),
	build: path.join(__dirname, 'dist/'),
	styles: path.join(__dirname, 'src', 'styles'),
	vendor: path.join(__dirname, 'node_modules'),
	src: path.join(__dirname, 'src'),
};

const common = {
	entry: {
		app: PATHS.app,
	},
	output: {
		path: PATHS.build,
		filename: '[name].js'
	},
	node: {
		fs: 'empty'
	}
};

var config;

var apiOptions = { api: { url: 'https://api.terramias.work', path: '/api'}}
if (process.env.API) {
	var apiData = process.env.API.match(/^(https?\:\/\/[^:\/?#]*(?:\:[0-9]+)?)([\/]{0,1}[^?#]*)$/);
	if(apiData) {
		apiOptions.api.url = apiData[1]
		apiOptions.api.path = apiData[2]
	}
}

// Detect how npm is run and branch based on that
switch(process.env.npm_lifecycle_event) {
	case 'build':
		config = merge(
			common,
			{
				output: {
					path: PATHS.build,
					filename: '[name].[hash:6].js',
				}
			},
			parts.setupProductionMode(),
			parts.generateMapSprites(PATHS),
			parts.modernizr(PATHS),
			parts.setupJsx(PATHS, apiOptions),
			parts.setupFonts(PATHS),
			parts.copyImages(PATHS),
			parts.setupImages(PATHS),
			parts.extractCSS(PATHS),
			parts.generateSprites(PATHS),
			parts.generateFavicons(PATHS),
			parts.generateHtml(PATHS),
			parts.injectVersion(),
            parts.offline(PATHS)
		);
		break;
	default:
		const devOptions = { host: '127.0.0.1', port: 4000 }
		if (process.env.HOST) {
			devOptions.host = process.env.HOST
		}
		if (process.env.PORT) {
			devOptions.port = process.env.PORT
		}
		config = merge(
			common,
			parts.devServer(PATHS, devOptions),
			{
				devtool: 'eval-source-map'
			},
			parts.modernizr(PATHS),
			parts.setupJsx(PATHS, apiOptions),
			parts.setupFonts(PATHS),
			parts.copyImages(PATHS),
			parts.setupImages(PATHS),
			parts.setupCSS(PATHS),
			parts.generateSprites(PATHS),
			parts.generateFavicons(PATHS),
			parts.generateHtml(PATHS),
			parts.injectVersion()
		);
}

module.exports = config;
import React from 'react'
import PropTypes from 'prop-types'
import TimerMixin from 'react-timer-mixin'
import reactMixin from 'react-mixin'

require('../../styles/characterbonus.scss')

@reactMixin.decorate(TimerMixin)
export default class CharacterBonus extends React.Component {
    /**
    * Define our prop types
    **/
    static propTypes = {
        character: PropTypes.object,
        settings: PropTypes.object
    }

    constructor() {
        super()
        this.state = {
            bonusClass: 'character-bonus',
            bonusWidth: '0%',
        }
    }

    componentDidMount() {
        this.setInterval(
            () => {
                const state = Object.assign(
                    {},
                    this.state
                )
                const minutes = (new Date()).getMinutes()
                if (minutes > 39) {
                    state.bonusClass = 'character-bonus bonus-time'
                } else {
                    state.bonusClass = 'character-bonus'
                }

                state.bonusWidth = (minutes / 3 * 5) + '%'
                this.setState(state)
            },
            20000
        )
    }

	render() {
		let { character, settings } = this.props

		return (
    <ol className={this.state.bonusClass}>
        <li className="character-bonus-games">{character.bonusGames}/{settings.maxBonusGames}</li>
        <li className="character-bonus-fights">{character.bonusFights}/{settings.maxBonusFights}</li>
        <li className="character-bonus-ducks">{character.bonusDucks}/{settings.maxBonusDucks}</li>
        <li className="character-bonus-timer"><span className="bar" style={{width: this.state.bonusWidth}}></span></li>
    </ol>
		);
	}
}
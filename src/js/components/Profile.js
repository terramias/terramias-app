import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Popup from './Popup'

@connect((store) => {
	return {
		character: store.api.character,
	};
})

export default class Profile extends Popup {
	constructor() {
		super()
	}

	render() {
		return (
			<div className="popup userProfile">
				<header>
					<h2>Mein Profil "{this.props.character.name}"</h2>
					<button onClick={this.close.bind(this)} className="button--image close"></button>
				</header>
				<section>
					<p></p>
				</section>
			</div>
		);
	}
}
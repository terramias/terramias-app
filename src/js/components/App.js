import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { setAxiosConfig } from 'redux-json-api'
import Route from 'react-router-dom/Route'
import Redirect from 'react-router/Redirect'
import Link from 'react-router-dom/Link'
import Modernizr from 'modernizr'

import { fetchSession } from '../actions/session'
import { fetchUser } from '../actions/user'
import { fetchSettings, fetchStatus } from '../actions/setting'

import Game from './Game'
import StartPage from './StartPage'
import Startup from './Startup'

require('../../styles/app.scss')

@connect(store => {
	return {
		user: store.user,
		session: store.session,
		setting: store.setting,
		status: store.status
	}
})

export default class App extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		user: PropTypes.object,
		session: PropTypes.object,
		dispatch: PropTypes.func,
		setting: PropTypes.object,
		status: PropTypes.object
	}

	constructor() {
		super()
		this.state = {
			redirect: false
		}
	}

	componentDidMount() {
		const { dispatch } = this.props

		if(localStorage.getItem('Authorization') !== undefined && localStorage.getItem('Authorization') !== null) {
			dispatch(setAxiosConfig({
				baseURL: 'API_URL' + 'API_PATH',
				timeout: 30000,
				headers: {'Authorization': localStorage.getItem('Authorization')}
			}))
		} else {
			dispatch(setAxiosConfig({
				baseURL: 'API_URL' + 'API_PATH',
				timeout: 30000,
				headers: {'Authorization': ''}
			}))
		}
		dispatch(fetchSettings()).then(() => {
			return dispatch(fetchSession())
		}).catch(() => {
			dispatch({ type: 'RESET_ERRORS' })
		})
	}

	componentWillReceiveProps(nextProps) {
		const { dispatch } = this.props
		const { user, session, location } = nextProps
		let redirect = false

		if (this.props.session !== session && session.authToken !== null && session.authToken !== '' && session.loggedIn) {
			if(localStorage.getItem('Authorization') !== session.authToken) {
				localStorage.setItem('Authorization', session.authToken);
				dispatch(setAxiosConfig({
					baseURL: 'API_URL' + 'API_PATH',
					timeout: 30000,
					headers: {'Authorization': session.authToken }
				}))
			}
			dispatch(fetchUser())
		} else if ((this.props.user !== user && user.id > 0) || (this.props.user === user && user.id > 0)) {
			if(user.status === 'active') {
				if(this.props.location.pathname.indexOf('/game') === -1) {
					redirect = '/game'
				}
			}
			if (user.status === 'new') {
				if (nextProps.location.pathname === '/start/login' || nextProps.location.pathname.indexOf('/start/') !== 0) {
					redirect = '/start/activate/' + user.email
				}
			}
		} else {
			if (nextProps.location.pathname === '/start/logout' || nextProps.location.pathname == '/start/' || nextProps.location.pathname.indexOf('/start/') !== 0) {
				redirect = '/start/login'
			}
			if (user.status === 'reactivate' && nextProps.location.pathname !== '/start/rules' && nextProps.location.pathname !== '/start/imprint') {
				if (nextProps.location.pathname.indexOf('/start/activate') !== 0) {
					redirect = '/start/reactivate'
				}
			}
		}

		if(this.state.redirect !== redirect) {
			this.setState({
				redirect: redirect
			})
		}
		return true
	}

	checkRequirements() {
		return Modernizr.promises === undefined || Modernizr.promises == null || Modernizr.promises === true
	}

	render() {
		let version = '[AIV]{version}[/AIV]'
		if(!this.checkRequirements()) {
			return <div className="app--error">
				<p>Dein Browser besitzt leider nicht die nötigen Vorraussetzungen um Terramias zu spielen.</p>
			</div>
		}

		return <Startup>
			<div className="app">
				<header className="app-header"></header>
				<main>
					<Route path="/start" component={StartPage} />
					{this.props.session.loggedIn ? <Route path="/game" component={Game}/> : null}
					{this.state.redirect ? (<Redirect to={this.state.redirect} />) : null }
				</main>
				<footer className="app-footer">
					<ul>
						<li>&copy; 2015-2017 Terramias Team</li>
						{this.props.session.loggedIn ? <li>Version <Link to="/game/changelog">{version}</Link></li> : <li>Version <Link to="/start/changelog">{version}</Link></li> }
						{this.props.session.loggedIn ? <li><Link to="/game/rules">Spielregeln</Link></li> : <li><Link to="/start/rules">Spielregeln</Link></li> }
					</ul>
				</footer>
			</div>
		</Startup>
	}
}
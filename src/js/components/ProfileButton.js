import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

import { getFileUrl } from '../actions/file'

require('../../styles/_imagebuttons.scss')

export default class ProfileButton extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		avatar: PropTypes.object,
		link: PropTypes.string,
		id: PropTypes.string,
	}

	render() {
		let image = null
		if(this.props.avatar.file && this.props.avatar.file.id !== 0) {
			image = <img src={getFileUrl(this.props.avatar.file.id)} className="avatar" style={{ width: '57px', height: '57px' }} alt="" />
		}
		return <Link to={this.props.link} className="button button--image character-profile" data-id={this.props.id}>
			<span className="avatar-button">
				{image}
			</span>
		</Link>
	}
}
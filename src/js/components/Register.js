import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Link from 'react-router-dom/Link'
import Formsy from 'formsy-react'

import { registerUser } from '../actions/user'
import { setFormData, resetFormData } from '../actions/formdata'
import { checkCharacterName } from '../actions/character'
import FormInput from './FormInput'
import FormCheckbox from './FormCheckbox'
import FormPasswordStrength from './FormPasswordStrength'
import FormButton from './FormButton'
import Popup from './Popup'

require('../../styles/register.scss')

@connect((store) => {
	return {
		error: store.error,
		user: store.user,
		formdata: store.formdata,
	}
})
export default class Register extends Popup {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		dispatch: PropTypes.func,
		error: PropTypes.string,
		password: PropTypes.string,
		email: PropTypes.string,
		params: PropTypes.object,
		user: PropTypes.object,
		formdata: PropTypes.object,
	}

	constructor() {
		super()
		this.state = {
			canSubmit: false,
			validationErrors: {},
			success: false,
			email: '',
			password: '',
			passwordRepeat: '',
			character: '',
			rulesCheck: false,
			isActive: true,
		}
		this.checkedNames = {}
		this.submit = this.submit.bind(this)
		this.close = this.close.bind(this)
		this.enableButton = this.enableButton.bind(this)
		this.disableButton = this.disableButton.bind(this)
		this.validateForm = this.validateForm.bind(this)
	}

	submit(data) {
		this.setState({
			isActive: false
		})
		this.validateForm(data)
		this.props.dispatch(registerUser({
			email: data.email,
			password: data.password,
			name: data.character,
			rules: data.rulesCheck
		})).then(() => {
			this.setState({
				success: true
			})
			resetFormData('register')
		}).catch(error => {
			this.setState({
				isActive: true,
				validationErrors: {
					email: 'Zu dieser Email existiert bereits ein Zugang.'
				}
			})
			console.error('API error', error)
		})
		return false
	}

	validateForm(values) {
		let errors = {}
		this.props.dispatch(setFormData('register', values))
		if(values.character && values.character.length > 1) {
			if(this.checkedNames.hasOwnProperty(values.character) && this.checkedNames[values.character] === true) {
				errors = {}
			} else if(this.checkedNames.hasOwnProperty(values.character) && this.checkedNames[values.character] === false) {
				errors.character = 'Der Name ist bereits vergeben'
			} else {
				this.props.dispatch(checkCharacterName({ name: values.character })).then(() => {
					this.checkedNames[values.character] = true
					errors = {}
				}).catch(() => {
					this.props.dispatch({ type: 'RESET_ERRORS' })
					this.checkedNames[values.character] = false
					errors.character = 'Der Name ist bereits vergeben'
				})
			}
		}
		if(!values.rulesCheck) {
			errors.rulesCheck = 'Bitte akzeptiere die Spielregeln'
		}

		this.setState({
			validationErrors: errors
		})
	}

	enableButton() {
		this.setState({ canSubmit: true })
	}

	disableButton() {
		this.setState({ canSubmit: false })
	}

	resetError() {
		if(this.props.error !== '') {
			this.props.dispatch({ type: 'RESET_ERRORS' })
		}
	}

	componentWillMount() {
		// check storage for form-data
		const { formdata } = this.props
		this.setState(Object.assign(
			{},
			this.state,
			formdata.register
		))
	}

	render() {
		return (
			<div className="popup popup--small register">
				<header style={{ display: (this.state.success ? 'none' : 'block') }}>
					<h2>Registrieren</h2>
					<button onClick={this.close} type="button" className="button--image close"></button>
				</header>
				<section style={{ display: (this.state.success ? 'none' : 'block') }}>
					<Formsy.Form onSubmit={this.submit} onValid={this.enableButton} onInvalid={this.disableButton} onChange={this.validateForm} validationErrors={this.state.validationErrors} className="register">
						<fieldset>
							<FormInput className="formdata--text" value={this.state.email} name="email" title="Emailadresse" validations="isEmail" validationError="Bitte gib eine gültige Emailadresse ein" required />
							<FormPasswordStrength className="formdata--text" value={this.state.password} name="password" title="Passwort" type="password" required validationError="Bitte gib ein Passwort ein" />
							<FormInput className="formdata--text" value={this.state.passwordRepeat} name="passwordRepeat" title="Wiederholung" type="password" validations="equalsField:password" required validationError="Das Passwort stimmt nicht mit dem ersten überein" />
							<FormInput className="formdata--text" value={this.state.character} name="character" title="Name" type="text" validations="minLength:2" required validationError="Bitte gib einen Namen ein" />
							<FormCheckbox className="formdata--checkbox" value={this.state.rulesCheck} name="rulesCheck" title={<span>Ich habe die <Link to="/start/rules">Spielregeln</Link> gelesen und stimme ihnen zu</span>} required validationError="Bitte bestätige, dass du die regeln gelesen hast" />
							<p className="formdata">
								<FormButton className="button button--bordered" type="submit" disabled={!this.state.canSubmit || !this.state.isActive} submitted={!this.state.isActive} title="Registrieren" />
							</p>
							<p className="formdata formdata--info">
								Du hast schon einen Zugang? <Link to="/start/login">Melde Dich an und leg los</Link>
							</p>
							<p className="formdata formdata--info">
								Du hast Dein Passwort vergessen? <Link to="/start/lostpassword">Erstelle ein neues Passwort</Link>.
							</p>
						</fieldset>
					</Formsy.Form>
				</section>
				<header style={{ display: (this.state.success ? 'block' : 'none') }}>
					<h2>Herzlich willkommen in Terramias</h2>
					<button onClick={this.close} type="button" className="button--image close"></button>
				</header>
				<section style={{ display: (this.state.success ? 'block' : 'none') }}>
					<div>
						<p>Wir haben Dir eine Email mit einem Aktivierungslink zugeschickt. Bitte öffne diesen Link, um Dich in Terramias zu aktivieren.</p>
					</div>
				</section>
			</div>
		);
	}
}

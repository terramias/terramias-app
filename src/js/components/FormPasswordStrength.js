import React from 'react'
import PropTypes from 'prop-types'
import { HOC } from 'formsy-react'
import zxcvbn from 'zxcvbn'

require('../../styles/passwordstrengthmeter.scss')

class FormPasswordStrength extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		className: PropTypes.string.isRequired,
		name: PropTypes.string.isRequired,
		title: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
		setValue: PropTypes.func.isRequired,
		isPristine: PropTypes.func.isRequired,
		isValid: PropTypes.func.isRequired,
		showError: PropTypes.func.isRequired,
		getErrorMessage: PropTypes.func.isRequired,
		getValue: PropTypes.func.isRequired,
		showRequired: PropTypes.func.isRequired,
	}

	constructor() {
		super()
		this.state = { width: '0%'}
		this.changeValue = this.changeValue.bind(this)
	}

	// setValue() will set the value of the component, which in turn will validate it and the rest of the form
	changeValue(event) {
		const result = zxcvbn(event.currentTarget.value)
		this.setState({width: (20 + result.score * 20) + '%'});
		this.props.setValue(event.currentTarget.value)
	}

	render() {

		// Set a specific className based on the validation
		// state of this component. showRequired() is true
		// when the value is empty and the required prop is
		// passed to the input. showError() is true when the
		// value typed is invalid
		const className = 'formdata password-strength' + (this.props.className ? ' ' + this.props.className : '') +
		(this.props.showRequired() ? ' formdata--required' : this.props.showError() ? ' formdata--invalid' : '') +
		(!this.props.isPristine() && this.props.isValid() ? ' formdata--valid' : '')

		// An error message is returned ONLY if the component is invalid
		// or the server has returned an error message
		const errorMessage = this.props.getErrorMessage()

		return <div className={className}>
			<label htmlFor={this.props.name}>{this.props.title}</label>
			<div className="formdata--input password-strength">
				<input type="password" id={this.props.name} name={this.props.name} value={this.props.getValue()} onChange={this.changeValue} formNoValidate />
				<div className="progress progress--animated progress--borderless progress-red progress--striped">
					<span className="bar" style={{ width: this.state.width }}></span>
				</div>
			</div>
			<span className='formdata--errorMessage'>{errorMessage}</span>
		</div>
	}
}
export default HOC(FormPasswordStrength)
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Popup from './Popup'

@connect((store) => {
	return {
		messages: store.api.messages,
	};
})

export default class Messaging extends Popup {
	constructor() {
		super()
		this.input = {
			search: '',
		}
	}

	render() {
		return (
			<div className="popup popup--large messageList">
				<header>
					<h2></h2>
					<button onClick={this.close.bind(this)} className="button--image close"></button>
				</header>
				<section>
					<p></p>
				</section>
			</div>
		);
	}
}


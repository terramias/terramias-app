import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { readEndpoint } from 'redux-json-api'
import Link from 'react-router-dom/Link'
import Route from 'react-router-dom/Route'

import ImageButton from './ImageButton'

import About from './About'
import Activate from './Activate'
import Changelog from './Changelog'
import Imprint from './Imprint'
import Login from './Login'
import Logout from './Logout'
import LostPassword from './LostPassword'
import Reactivate from './Reactivate'
import Register from './Register'
import ResetPassword from './ResetPassword'
import Rules from './Rules'
import Support from './Support'

import { finishStartup } from '../actions/app'

require('../../styles/startpage.scss')


@connect((store) => {
	return {
		setting: store.api.setting,
		user: store.user,
	};
})

export default class StartPage extends React.Component {
	componentWillMount() {
		const { dispatch } = this.props
		dispatch(finishStartup())
	}

	render() {
		const { match } = this.props
		return (
			<section className="startPage">
				<div className="compass"></div>
				<aside className="sidebar">
				    <nav>
				        <ul>
				            <li>
				            	<Link to="/start/register">Registrieren</Link>
				            </li>
				            <li>
				            	<Link to="/start/login">Login</Link>
				            </li>
				            <li>
				            	<Link to="/start/about">Info</Link>
				            </li>
				            <li>
				            	<Link to="/start/imprint">Impressum</Link>
				            </li>
				        </ul>
				    </nav>
				</aside>
				<Route path={`${match.url}/login`} component={Login} />
				<Route path={`${match.url}/logout`} component={Logout} />
				<Route path={`${match.url}/register`} component={Register} />
				<Route path={`${match.url}/lostpassword/:email?`} component={LostPassword} />
				<Route path={`${match.url}/resetpassword/:email?/:token?`} component={ResetPassword} />
				<Route path={`${match.url}/activate/:email?/:token?`} component={Activate} />
				<Route path={`${match.url}/reactivate/:email?`} component={Reactivate} />
				<Route path={`${match.url}/about`} component={About} />
				<Route path={`${match.url}/imprint`} component={Imprint} />
				<Route path={`${match.url}/rules`} component={Rules} />
				<Route path={`${match.url}/support`} component={Support} />
			</section>
		);
	}
}
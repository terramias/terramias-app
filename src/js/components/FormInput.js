import React from 'react'
import PropTypes from 'prop-types'
import { HOC } from 'formsy-react'

class FormInput extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		className: PropTypes.string,
		name: PropTypes.string.isRequired,
		type: PropTypes.string,
		placeholder: PropTypes.string,
		title: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
		setValue: PropTypes.func.isRequired,
		isPristine: PropTypes.func.isRequired,
		isValid: PropTypes.func.isRequired,
		showError: PropTypes.func.isRequired,
		getErrorMessage: PropTypes.func.isRequired,
		getValue: PropTypes.func.isRequired,
		showRequired: PropTypes.func.isRequired,
		onChange: PropTypes.func,
	}

	constructor() {
		super()
		this.changeValue = this.changeValue.bind(this)
		this.blurValue = this.blurValue.bind(this)
		this.focusValue = this.focusValue.bind(this)
		this.keyDown = this.keyDown.bind(this)
		this.state = {
			hasFocus: false
		}
	}

	// setValue() will set the value of the component, which in
	// turn will validate it and the rest of the form
	changeValue(event) {
		this.props.setValue(event.currentTarget[this.props.type === 'checkbox' ? 'checked' : 'value'])
		if(this.props.onChange) {
			this.props.onChange( {
				id: this.props.name,
				value: event.currentTarget.value
			})
		}
	}

	focusValue() {
		this.setState({
			hasFocus: false
		})
	}

	blurValue(event) {
		this.setState({
			hasFocus: false
		})
		this.props.setValue(event.currentTarget[this.props.type === 'checkbox' ? 'checked' : 'value'])
	}

	keyDown(event) {
		if(event.keyCode=='13'){
			this.props.setValue(event.currentTarget[this.props.type === 'checkbox' ? 'checked' : 'value'])
		}
	}

	render() {

		// Set a specific className based on the validation
		// state of this component. showRequired() is true
		// when the value is empty and the required prop is
		// passed to the input. showError() is true when the
		// value typed is invalid
		const className = 'formdata' + (this.props.className ? ' ' + this.props.className : '') +
		(this.props.showRequired() ? ' formdata--required' : this.props.showError() ? ' formdata--invalid' : '') +
		(!this.props.isPristine() && this.props.isValid() ? ' formdata--valid' : '')

		// An error message is returned ONLY if the component is invalid
		// or the server has returned an error message
		let errorMessage = null
		if(this.props.getErrorMessage()) {
			errorMessage = <span className="formdata--errorMessage">{this.props.getErrorMessage()}</span>
		}
		if(this.state.hasFocus) {
			errorMessage = null
		}

		return <div className={className}>
			<label htmlFor={this.props.name}>{this.props.title}</label>
			<input type={this.props.type || 'text'} name={this.props.name} id={this.props.name} onBlur={this.blurValue} onFocus={this.focusValue} onKeyDown={this.keyDown} onChange={this.changeValue} value={this.props.getValue()} checked={this.props.type === 'checkbox' && this.props.getValue() ? 'checked' : null}  placeholder={this.props.placeholder} formNoValidate />
			{errorMessage}
		</div>
	}
}
export default HOC(FormInput)
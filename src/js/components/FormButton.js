//require('grecaptcha')

import React from 'react'
import PropTypes from 'prop-types'

import Spinner from './Spinner'

export default class FormButton extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		className: PropTypes.string,
		type: PropTypes.string,
		submitted: PropTypes.bool,
		disabled: PropTypes.bool,
		title: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
	}

	constructor() {
		super()
		this.state = {
			disabled: true,
			grecaptchaPassed: false
		}
		window.grecaptchaCallback = this.grecaptchaCallback.bind(this)
	}

	componentWillReceiveProps(nextProps) {
		if(this.props.disabled !== nextProps.disabled) {
			this.setState({
				disabled: nextProps.disabled// || !this.state.grecaptchaPassed
			})
		}
	}

	grecaptchaCallback() {
		this.setState({
			grecaptchaPassed: true,
			disabled: this.props.disabled || false
		})
	}

	render() {
		//const className = 'g-recaptcha formdata' + (this.props.className ? ' ' + this.props.className : '')
		const className = 'formdata' + (this.props.className ? ' ' + this.props.className : '')

		//		return <button data-sitekey="6LcRBg4UAAAAAOlHfTDtrefplJOqD059ST-unpw-" data-callback="grecaptchaCallback" data-size="invisible" className={className} type={this.props.type || 'submit'} disabled={this.state.disabled}>
		return <button className={className} type={this.props.type || 'submit'} disabled={this.state.disabled}>
			{this.props.submitted ? <Spinner /> : this.props.title}
		</button>
	}
}

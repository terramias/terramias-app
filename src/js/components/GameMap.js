import React from 'react'
import PropTypes from 'prop-types'
import { TweenLite } from 'gsap'
import screenfull from 'screenfull'
import SAT from 'sat'

var PIXI = require('pixi.js')
require('pixi-display')

import Player from '../map/Player'
//import PathEditor from '../map/PathEditor'
import InfoBubbles from '../map/InfoBubbles'
import LandInfo from '../map/LandInfo'
import PathFinder from '../map/PathFinder'
// import { Router } from 'Router'
import { getImageUrl } from '../actions/file'

require('../../styles/map.scss')

export default class GameMap extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		character: PropTypes.object.isRequired,
		figure: PropTypes.object.isRequired,
		land: PropTypes.object.isRequired,
		maps: PropTypes.array.isRequired,
		portals: PropTypes.object.isRequired,
		position: PropTypes.object.isRequired,
		tooltips: PropTypes.array.isRequired,
		ways: PropTypes.array.isRequired,
		waypoints: PropTypes.object.isRequired,
		onPortal: PropTypes.func,
		onUpdate: PropTypes.func,
		onClick: PropTypes.func,
	}

	/**
	 * Constructor
	 *
	 * @return {null}
	 */
	constructor(props: Object) {
		super(props)

		this.ready = false
		this.portals = []
		this.animate = this.animate.bind(this)
		this.onClickHandler = this.onClickHandler.bind(this)
		this.onPlayerMoveHandler = this.onPlayerMoveHandler.bind(this)
		this.onPortalHandler = this.onPortalHandler.bind(this)

		this.onResizeHandler = this.onResizeHandler.bind(this)
		this.onKeyDownHandler = this.onKeyDownHandler.bind(this)
		this.onKeyUpHandler = this.onKeyUpHandler.bind(this)
		this.onMouseOverHandler = this.onMouseOverHandler.bind(this)
		this.state = {
			keys: {},
			scale: 1,
			mousePosition: { x: 0, y: 0 }
		}
	}

	/**
	* Create the map-structure
	*
	* @return {null}
	**/
	componentDidMount() {
		const size = this.getContainerSize()
		const baseLayer = new PIXI.DisplayGroup(-1, false)
		const playerLayer = new PIXI.DisplayGroup(1, false)
		const overlayLayer = new PIXI.DisplayGroup(2, false)
		const loaderLayer = new PIXI.DisplayGroup(3, false)

		if (screenfull.enabled) {
			document.addEventListener(screenfull.raw.fullscreenchange, this.onResizeHandler)
		}

		document.addEventListener('keydown', this.setKey)
		document.addEventListener('keyup', this.resetKey)
		window.addEventListener('resize', this.onResizeHandler)

		this.renderer = PIXI.autoDetectRenderer(
			size.width,
			size.height,
			{
				antialias: false,
				autoResize: true
			}
		)
		this.size = size
		this.node.appendChild(this.renderer.view)

		this.stage = new PIXI.Container()
		this.stage.interactive = true
		this.stage.mousemove = this.onMouseOverHandler
		this.stage.scale.x = this.state.scale
		this.stage.scale.y = this.state.scale
		this.stage.click = this.onClickHandler
		this.stage.tap = this.onClickHandler
		this.stage.displayList = new PIXI.DisplayList()

		this.map = new PIXI.Container()
		this.stage.addChild(this.map)

		this.baseMap = new PIXI.Container()
		this.baseMap.displayGroup = baseLayer
		this.map.addChild(this.baseMap)

		this.player = new Player()
		this.player.displayGroup = playerLayer
		this.player.move = this.onPlayerMoveHandler
		this.map.addChild(this.player)

		this.overlayMap = new PIXI.Container()
		this.overlayMap.displayGroup = overlayLayer
		this.map.addChild(this.overlayMap)

		if(localStorage.getItem('debug')) {
			this.debugMap = new PIXI.Container()
			this.map.addChild(this.debugMap)
		}

		this.router = new PathFinder()

		this.animate()
	}

	/**
	 * Cleanup after removing the map from the view
	 *
	 * @return {null}
	 */
	componentWillUnmount() {
		document.removeEventListener(screenfull.raw.fullscreenchange, this.onResizeHandler)
		document.removeEventListener('keydown', this.setKey)
		document.removeEventListener('keyup', this.resetKey)
		window.removeEventListener('resize', this.onResizeHandler)

		this.ready = false
	}

	/**
	 * Eventhandler for the keydown-events
	 *
	 * @param {Object} event Keyboard-event
	 * @return {null}
	 */
	onKeyDownHandler(event: Object) {
		if(this.ready) {
			this.setState({
				keys: Object.assign({}, this.state.keys, { [event.keyCode]: true })
			})
		}
	}

	/**
	 * Eventhandler for the keyup-events
	 *
	 * @param {Object} event Keyboard-event
	 *
	 * @return {null}
	 */
	onKeyUpHandler(event: Object) {
		if(this.ready) {
			this.setState({
				keys: Object.assign({}, this.state.keys, { [event.keyCode]: false })
			})
		}
	}

	/**
	 * Eventhandler for the window-resize-event
	 *
	 * @return {null}
	 */
	onResizeHandler() {
		const size = this.getContainerSize()
		if ((this.renderer.width !== size.width || this.renderer.height !== size.height) || (this.size.width !== size.width || this.size.height !== size.height)) {
			this.resizeMaps(size)
		}
	}

	/**
	 * shouldComponentUpdate is used to check our new props against the current
	 * and only update if needed
	 *
	 * @return {null}
 	 **/
	shouldComponentUpdate(nextProps: Object, nextState: Object) {
		if(nextProps.maps !== this.props.maps) {
			return true
		}

		if(nextProps.figure !== this.props.figure) {
			return true
		}

		if(nextProps.land !== this.props.land) {
			return true
		}

		if(nextProps.character !== this.props.character) {
			return true
		}

		if(nextProps.tooltips !== this.props.tooltips) {
			return true
		}

		if(nextState.mousePosition !== this.state.mousePosition) {
			return true
		}

		if(nextState.position !== this.state.position) {
			return true
		}

		return false
	}

	/**
	 * Update the rendered view with the next properties
	 *
	 * @param  {Object} nextProps	Object with the next properties
	 * @return {null}
	 */
	componentWillReceiveProps(nextProps: Object) {
		const size = this.getContainerSize()
		if (this.size.width !== size.width || this.size.height !== size.height) {
			this.size = size
		}

		if(nextProps.maps !== this.props.maps) {
			this.renderMaps(nextProps.maps)
			this.moveMap(Math.round(size.width / 2 - this.props.position.x), Math.round(size.height / 2 - this.props.position.y), -1)
			new TweenLite(this.mapLoader.style, 1.5, { opacity: 0, overwrite: 'all', ease: 'Linear.easeNone' })
			window.setTimeout(() => {
	  			this.mapLoader.style.display = 'none'
			}, 2500)
		}
		if(nextProps.ways !== this.props.ways) {
			this.setupWays(nextProps.ways, nextProps.waypoints)
		}

		if(nextProps.portals !== this.props.portals) {
			this.setupPortals(nextProps.portals)
		}

		if(nextProps.land !== this.props.land) {
			this.setupLand(nextProps.land)
		}

		if(nextProps.position !== this.props.position) {
			if(nextProps.position.land_id !== this.props.position.land_id) {
				console.info('new position', nextProps.position)
				this.createSnapshot({ addToMap: true })
				this.resetMap()
				let playerPosition = Object.assign({}, nextProps.position)
				this.player.setPosition(playerPosition)
			}
			//this.moveMap(size.width / 2 - nextProps.position.x, size.height / 2 - nextProps.position.y, 0)
		}

		if(nextProps.figure !== this.props.figure) {
			this.player.setFigure(nextProps.figure)
		}
	}

	/**
	 * Animation function for the requestframe
	 *
	 * @return {null}
	 */
	animate() {
		this.renderer.render(this.stage)
		requestAnimationFrame( this.animate )
	}

	/**
	 * Create a snapshot of the current stage
	 */
	createSnapshot( { addToMap = false }) {
		let renderTexture = PIXI.RenderTexture.create(this.renderer.width, this.renderer.height)
	  	this.renderer.render(this.stage, renderTexture, null, null, true)

  		if(addToMap) {
  			this.mapLoader.style.opacity = 1
  			this.mapLoader.style.display = 'block'
  			while(this.mapLoader.firstChild) {
  				this.mapLoader.removeChild(this.mapLoader.firstChild)
  			}
  			this.mapLoader.appendChild(this.renderer.extract.image(renderTexture))
  		} else {
  			return this.renderer.extract.image(renderTexture)
  		}
	}

	/**
	 * Animation function for the requestframe
	 *
	 * @return {null}
	 */
	getContainerSize() {
		return {
			width: document.querySelector('.map').clientWidth,
			height: document.querySelector('.map').clientHeight
		}
	}

	/**
	 * Animation function for the requestframe
	 *
	 * @return {null}
	 */
	onClickHandler(event: Object) {
		const targetPosition = event.data.global
		targetPosition.x -= this.stage.x
		targetPosition.y -= this.stage.y
		console.log('CLICK', targetPosition)
		const path = this.router.getRoute(this.player.position.x, this.player.position.y, targetPosition.x, targetPosition.y)
		if(path.length > 0) {
			this.player.setTargets(path)
		} else {
			this.player.moveTo(targetPosition)
		}

	}

	/**
	 * Animation function for the requestframe
	 *
	 * @return {null}
	 */
	onPlayerMoveHandler(finished = false) {
		let moveMap = false

		if (this.player.position.x + this.stage.position.x > 0.85 * this.size.width) {
			moveMap = true
		}
		if (this.player.position.y + this.stage.position.y > 0.85 * this.size.height) {
			moveMap = true
		}
		if (this.player.position.x + this.stage.position.x < 0.15 * this.size.width) {
			moveMap = true
		}
		if (this.player.position.y + this.stage.position.y < 0.15 * this.size.height) {
			moveMap = true
		}


		if(moveMap && !this.player.block) {
			this.moveMap(Math.round(this.size.width / 2 - this.player.position.x), Math.round(this.size.height / 2 - this.player.position.y), 1)
		}

		if(finished && !this.player.block) {
			this.props.onUpdate({
				id: this.props.character.id,
				x: this.player.position.x,
				y: this.player.position.y
			}).then(() => {
				if(this.checkCollisions(this.player.position)) {
					this.player.stop()
				}
			})
		} else {
			if(this.checkCollisions(this.player.position)) {
				this.player.stop()
			}
		}
	}

	onMouseOverHandler(event: Object) {
		const targetPosition = event.data.global
		targetPosition.x -= this.stage.x
		targetPosition.y -= this.stage.y

		if(
			targetPosition.x - this.state.mousePosition.x > 10
		||
			targetPosition.x - this.state.mousePosition.x < -10
		||
			targetPosition.y - this.state.mousePosition.y > 10
		||
			targetPosition.y - this.state.mousePosition.y < -10
		) {
			if(this.ready) {
				this.setState({
					mousePosition: Object.assign({}, targetPosition)
				})
			}
		}
	}

	onPortalHandler(portal: Object) {
		this.props.onPortal(portal)
	}

	/**
	 * Move the map to the given position
	 *
	 * @return {null}
	 */
	moveMap(x: Number, y: Number, time = null) {
		if (this.stage.width < this.size.width) {
			x = Math.round(this.size.width / 2 - this.stage.width / 2)
		} else {
			x = Math.max(this.size.width - this.stage.width, Math.min(0, x))
		}

		if(this.stage.height < this.size.height) {
			y = Math.round(this.size.height / 2 - this.stage.height / 2)
		} else {
			y = Math.max(this.size.height - this.stage.height, Math.min(0, y))
		}

		console.log('map', x, y, time)
		if(time <= 0) {
			this.stage.position.x = x
			this.stage.position.y = y
		} else {
			if(!time) {
				time = Math.sqrt(
					(this.stage.position.x - x) * (this.stage.position.x - x) +
					(this.stage.position.y - y) * (this.stage.position.y - y)
				)
				time = Math.round(time / 133)
			}
			this.tween = new TweenLite(this.stage.position, time, { x, y, overwrite: 'all', ease: 'Linear.easeNone' })
		}
	}

	/**
	 * Resize the map and center it to the players position
	 *
	 * @param  {object} size Object with the width and height of the map-view
	 * @return {null}
	 */
	resizeMaps(size: Object) {
		this.size = size
		this.renderer.resize(size.width, size.height)
		this.moveMap(Math.round(this.size.width / 2 - this.player.position.x), Math.round(this.size.height / 2 - this.player.position.y), 1)
	}

	/**
	 * Check the given position against all portals for a collision
	 *
	 * @param  {Number} x Position to check for collisions
	 * @param  {Number} y Position to check for collisions
	 * @return {null}
	 */
	checkCollisions({ x, y }) {
		const player = this.player.getBox()
		player.pos = new SAT.Vector(x, y)

		const response = new SAT.Response();
		let collisions = this.portals.filter(portal => {
			return SAT.testPolygonPolygon(player, portal.polygon, response)
		})
		if(collisions.length > 0) {
			collisions.forEach(portal => {
				this.onPortalHandler(portal)
			})
			return true
		}
		return false

	}

	/**
	 * Render the map-layers
	 *
	 * @param  {array} maps Array of maps
	 * @return {null}
	 */
	renderMaps(maps: Array) {
		maps.forEach( map => {
			const baseSprite = new PIXI.Sprite.fromImage(getImageUrl('map', 'base/' + map.baseImage));
			baseSprite.height = map.height
			baseSprite.width = map.width
			baseSprite.x = map.x
			baseSprite.y = map.y
			this.baseMap.addChild(baseSprite)

			if(map.layerImage === '') {
				return
			}
			const overlaySprite = new PIXI.Sprite.fromImage(getImageUrl('map', 'layer/' + map.layerImage));
			overlaySprite.height = map.height
			overlaySprite.width = map.width
			overlaySprite.x = map.x
			overlaySprite.y = map.y
			this.overlayMap.addChild(overlaySprite)
		})
		this.ready = true
	}

	/**
	 * Reset the map for a change of the land
	 *
	 * @return {null}
	 */
	resetMap() {
		console.log('RESET MAP')
		this.baseMap.removeChildren()
		this.overlayMap.removeChildren()
		if(localStorage.getItem('debug')) {
			this.debugMap.removeChildren()
		}
	}

	/**
	 * Add all portals of the current map to the collision-map
	 *
	 * @params {Object} portals Object with all portals of the current map
	 * @return {null}
	 */
	setupPortals(portals: Object) {
		const graphics = new PIXI.Graphics();
		graphics.lineStyle(3, 0xff0000, 0.5)

		this.portals = []
		Object.keys(portals).forEach(key => {
			if(localStorage.getItem('debug')) {
				graphics.beginFill(0xff0000, 0.5)
				graphics.drawRect(
					portals[key].x,
					portals[key].y,
					portals[key].width,
					portals[key].height
				)
				graphics.endFill()
			}
			this.portals.push({
				id: portals[key].id,
				polygon: new SAT.Polygon(
					new SAT.Vector(portals[key].x, portals[key].y),
					[
						new SAT.Vector(),
						new SAT.Vector(0, portals[key].height),
						new SAT.Vector(portals[key].width, portals[key].height),
						new SAT.Vector(portals[key].width, 0)
					]
				)
			})
		})

		if(localStorage.getItem('debug')) {
			this.debugMap.addChild(graphics)
		}
	}

	setupLand(land) {
		this.renderer.backgroundColor = parseInt(land.background.replace('#', '0x'), 16)
	}

	setupWays(ways, waypoints) {
		ways = ways.map(way => {
			way.start = waypoints[way.start]
			way.end = waypoints[way.end]
			return way
		})

		this.router.update( {
			width: this.stage.width,
			height: this.stage.height,
			ways: ways
		})

		if(localStorage.getItem('debug')) {
			const graphics = new PIXI.Graphics();
			graphics.lineStyle(7, 0xff0000, 0.5)

			ways.forEach(way => {
				graphics.beginFill(0x00aa00, 0.5)
				graphics.moveTo(
					way.start.x,
					way.start.y
				)
				graphics.lineTo(
					way.end.x,
					way.end.y
				)
				graphics.endFill()
			})

			this.debugMap.addChild(graphics)
/*
		Object.keys(waypoints).forEach(key => {
			const text = new PIXI.extras.BitmapText(String(key))
			text.x = waypoints[key].x
			text.y = waypoints[key].y
			this.debugMap.addChild(text)
		})
*/
		}
	}

	/**
	 * Render the DOM for the map
	 *
	 * @return {null}
	 */
	render() {
		return <div className="map" ref={node => this.node = node}>
			<LandInfo land={this.props.land} />
			<InfoBubbles tooltips={this.props.tooltips} position={this.state.mousePosition} stage={(this.stage ? this.stage.position : {x: 0, y: 0})} />
			<div ref={element => { this.mapLoader = element }} className="map-loader"></div>
		</div>
	}
}
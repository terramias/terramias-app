import React from 'react'
import PropTypes from 'prop-types'
import Popup from './Popup'

require('../../styles/about.scss')

export default class Options extends Popup {
	/**
	 * Define our prop types
	 **/
	static propTypes = {}

	render() {
		return (
			<div className="popup popup--big">
				<header>
					<h2>Options</h2>
					<button onClick={this.close.bind(this)} className="button--image close"></button>
				</header>
				<section>
					<p></p>
				</section>
			</div>
		);
	}
}

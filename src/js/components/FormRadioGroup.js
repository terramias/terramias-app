import React from 'react'
import PropTypes from 'prop-types'
import { HOC } from 'formsy-react'

class FormRadioGroup extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
		className: PropTypes.string,
		title: PropTypes.oneOfType([PropTypes.string, PropTypes.object]),
		name: PropTypes.string.isRequired,
		type: PropTypes.string,
		items: PropTypes.array.isRequired,
		setValue: PropTypes.func.isRequired,
		isPristine: PropTypes.func.isRequired,
		isValid: PropTypes.func.isRequired,
		showError: PropTypes.func.isRequired,
		getErrorMessage: PropTypes.func.isRequired,
		getValue: PropTypes.func.isRequired,
		showRequired: PropTypes.func.isRequired,
		onChange: PropTypes.func,
	}

	constructor() {
		super()
		this.state = {
			value: ''
		}
	}

	componentWillMount() {
		const value = this.props.value
		this.props.setValue(value)
		this.setState({ value })
	}

	changeValue(value) {
		this.props.setValue(value)
		this.setState({ value })
		if(this.props.onChange) {
			this.props.onChange( {
				id: this.props.name,
				value
			})
		}
	}

	render() {
		const className = 'form-group' + (this.props.className || ' ') +
			(this.props.showRequired() ? 'required' : this.props.showError() ? 'error' : '')
		const errorMessage = this.props.getErrorMessage()

		const { name, title, items } = this.props
		return (
			<div className={className}>
				<div>{title}</div>
				{items.map((item, i) => (
					<label key={i} style={(item.style ? item.style : {})}>
						{item.order && item.order === 'left' ? (<span style={item.styleLabel ? item.styleLabel : {}}>{item.label}</span>) : null}
						<input type="radio" name={name} onChange={this.changeValue.bind(this, item.value)} checked={this.state.value == item.value} />
						{item.order && item.order === 'left' ? null : (<span style={item.styleLabel ? item.styleLabel : {}}>{item.label}</span>)}
					</label>
				))
				}
				<span className='validation-error'>{errorMessage}</span>
			</div>
		)
	}

}
export default HOC(FormRadioGroup)
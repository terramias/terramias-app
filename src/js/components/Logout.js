import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { setHeader } from 'redux-json-api'
import Redirect from 'react-router/Redirect'

import { resetUser } from '../actions/user'
import { deleteSession } from '../actions/session'

@connect(store => {
	return {
		user: store.user,
		session: store.session,
		setting: store.setting,
	}
})
export default class Logout extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		user: PropTypes.object,
		dispatch: PropTypes.func,
		setting: PropTypes.object,
		session: PropTypes.object,
	}

	componentWillMount() {
		const { dispatch } = this.props

		localStorage.removeItem('Authorization')
		dispatch(deleteSession(this.props.session)).then(() => {
			return dispatch(resetUser)
		}).then(() => {
			dispatch(setHeader({
				Authorization: ''
			}))
		})
	}

	render() {
		return ( <Redirect to="/start/login" /> )
	}
}
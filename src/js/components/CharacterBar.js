import React from 'react'
import PropTypes from 'prop-types'
import Link from 'react-router-dom/Link'

require('../../styles/characterbar.scss')

export default class CharacterBar extends React.Component {
	/**
	* Define our prop types
	**/
	static propTypes = {
		character: PropTypes.object,
		onClickHandler: PropTypes.func,
	}

	onClick() {
		this.props.onClickHandler()
	}
	render() {
		let { character } = this.props
		return (
<aside className="character-bar">
	<div className="character-refresh progress progress--striped progress--vertical progress--3d">
		<span className="bar" style={{height: (40 + character.levelPoints) + '%'}}></span>
	</div>
	<span className="character-name"><Link to={'/game/character/' + character.id}>{character.name}</Link></span>
	<span className="character-day">Spieltag: {character.generationDays}</span>
	<span className="character-level">Level: {character.level}</span>
	<div className="character-need-1 progress progress--striped progress--3d">
		<span className="bar" style={{width: (50 + character.levelPoints) + '%'}}></span>
	</div>
	<div className="character-need-2 progress progress--striped progress--3d">
		<span className="bar" style={{width: (30 + character.levelPoints) + '%'}}></span>
	</div>
	<div className="character-progress progress progress--striped progress--3d">
		<span className="bar" style={{width: (70 + character.levelPoints) + '%'}}></span>
	</div>
</aside>
		);
	}
}
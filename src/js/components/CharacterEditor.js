import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Dropzone from 'react-dropzone'

import { fetchFigures } from '../actions/figure'
import { createAvatar } from '../actions/avatar'
import { uploadFile, getFileUrl } from '../actions/file'
import { updateCharacter } from '../actions/character'

require('../../styles/charactereditor.scss')

@connect((store) => {
	return {
		character: store.character,
		user: store.user,
		avatars: store.avatars,
		avatar: store.avatar,
		figures: store.figures,
		figure: store.figure,
	}
})

export default class CharacterEditor extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		avatars: PropTypes.array,
		avatar: PropTypes.object,
		figure: PropTypes.object,
		figures: PropTypes.array,
		dispatch: PropTypes.func,
		character: PropTypes.object,
	}

	constructor() {
		super()
		this.close = this.close.bind(this)
		this.save = this.save.bind(this)
		this.onDrop = this.onDrop.bind(this)
		this.onOpenClick = this.onOpenClick.bind(this)
		this.handleChange = this.handleChange.bind(this)
		this.state = {
			character: {
				id: 0,
				name: '',
				description: '',
				figureId: 1,
				avatarId: 0,
				avatarImage: 'img/gui/ui-elements/avatar_profil.png',
				gender: 'w',
			}
		}
	}

	save() {
		const { dispatch } = this.props

		dispatch(updateCharacter(this.state.character)).then(() => {
			this.close()
		})
	}

	close() {
		console.info(this.props)
		this.props.history.goBack()
	}

	onDrop(files) {
		let fileInfo = {
			preview: files[0].preview,
			name: files[0].name,
			filesize: files[0].size,
			contentType: files[0].type
		}
		this.setState({ character: Object.assign({}, this.state.character, {avatarImage: fileInfo.preview})})

		this.props.dispatch(createAvatar()).then(response => {
			const fileId = response.body.data.relationships.file.data.id
			this.setState({
				character: Object.assign(
					{},
					this.state.character,
					{ avatarId: this.props.avatar.id }
				)
			})
			return this.props.dispatch(uploadFile(fileId, files[0]))
		}).then(() => {
			console.info(arguments)
		}).catch(error => {
			console.error(error)
		})
	}

    onOpenClick() {
      this.dropzone.open();
    }

	componentWillMount() {
		const { dispatch } = this.props
		const character = Object.assign({}, this.state.character)

		if (this.props.character) {
			Object.keys(character).forEach(key => {
				if(!this.props.character[key]) {
					return
				}
				character[key] = this.props.character[key]
			})
		}
		if(this.props.avatar) {
			character.avatar = this.props.avatar
		}
		if(this.props.figure) {
			character.figure = this.props.figure
		}

		if(character.avatar && character.avatar.file && character.avatar.file.id) {
			character.avatarImage = getFileUrl(character.avatar.file.id)
		} else if(character.avatarId) {
			character.avatarImage = getFileUrl(character.avatarId)
		} else {
			character.avatarImage = this.state.character.avatarImage
		}

		this.setState({ character })
		console.log(character)

		dispatch(fetchFigures())
	}

	handleChange(event) {
		this.setState({ character: Object.assign(
				{},
				this.state.character,
				{ [event.target.name]: event.target.value }
			)}
		)
		return true
	}

	render() {
		let figures = []
		Object.keys(this.props.figures).forEach(id => {
			const figure = this.props.figures[id]

			if(figure.gender !== this.state.character.gender) {
				return
			}

			figures.push(<label key={'figure-' + figure.id} style={{ display: 'inline-block', margin: '0 10px', padding: '0 20px' }}>
				<input type="radio" name="figureId" value={figure.id} checked={this.state.character.figureId === figure.id} onChange={this.handleChange} />
				<br />
				<img src={figure.image.split('/sprites/').join('/figures/')} />
			</label>)
		})
		return (
			<div className="popup character-editor">
				<Dropzone ref={(node) => { this.dropzone = node; }} accept="image/*" style={{}} activeStyle={{}} disableClick={true} onDrop={this.onDrop}>
					<header>
						<h2>{(this.state.character.name !== '' ? 'Herzlich willkommen "' + this.state.character.name  + '"' : 'Charakter erstellen')}</h2>
						<button onClick={this.close} type="button" className="button--image close"></button>
					</header>
					<section style={{ textAlign: 'center'}}>
						<p>Du bist nur noch einen kleinen Schritt vom Spiel entfernt.<br />Erstelle Dir hier Deinen Charakter für Terramias:</p>
						<div className="character-avatar-box" style={{ textAlign: 'center' }}>
							<div style={{ margin: '10px auto 5px auto', border: '1px solid #000', width: '105px', height: '105px' }} onClick={this.onOpenClick}>
								<img src={this.state.character.avatarImage} style={{ width: '100%', height: '100%' }}/>
							</div>
							<button type="button" className="button button--bordered" onClick={this.onOpenClick}>Avatar hochladen</button>
						</div>
						<label>
							<h4>Wähle Deinen Namen:</h4>
							<p>
								<input type="text" name="name" defaultValue={this.state.character.name} onChange={this.handleChange} />
							</p>
						</label>
						<h4>Wähle Dein Geschlecht:</h4>
						<p>
							<label>weiblich <input type="radio" name="gender" value="w" checked={this.state.character.gender === 'w'} onChange={this.handleChange} /></label>
							<label><input type="radio" name="gender" value="m" checked={this.state.character.gender === 'm'} onChange={this.handleChange} /> männlich</label>
						</p>
						<h4>Wähle Dein Spielfigur aus:</h4>
						<p>
						{figures}
						</p>
					</section>
					<footer style={{textAlign: 'right'}}>
						<button type="button" style={{ display: (this.state.character.id > 0 ? 'inline-block' : 'none')}} className="button button--bordered button--primary" onClick={this.save}>Speichern</button>
						<button type="button" style={{ display: (this.state.character.id == 0 ? 'inline-block' : 'none')}} className="button button--bordered button--primary" onClick={this.create}>Weiter zum Spiel</button>
					</footer>
				</Dropzone>
			</div>
		)
	}
}
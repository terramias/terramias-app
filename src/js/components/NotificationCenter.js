import React from 'react'
import PropTypes from 'prop-types'

import Notification from './Notification'

require('../../styles/notificationcenter.scss')

class NotificationCenter extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		notifications: PropTypes.object,
	}

	render() {
		return <div className="notification-center">
			<ol className="notification-list">
				{Object.keys(this.props.notifications).map(key => {
					return <li key={key}><Notification notification={this.props.notifications[key]} /></li>
				})}
			</ol>
		</div>
	}
}

export default NotificationCenter
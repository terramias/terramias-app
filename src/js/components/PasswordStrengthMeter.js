import React from 'react'
import PropTypes from 'prop-types'
import zxcvbn from 'zxcvbn'

require('../../styles/passwordstrengthmeter.scss')

export default class PasswordStrengthMeter extends React.Component {
	constructor() {
		super()
		this.state = { width: '0%'}
		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(event) {
		const result = zxcvbn(event.target.value)
		this.setState({width: (20 + result.score * 20) + '%'});

		this.props.onChangeHandler(event);
		
		return true
	}

	render() {

		return <div className="input password-strength">
			<input type="password" name={this.props.name} id={this.props.id} onChange={this.handleChange} />
			<div className="progress progress--animated progress--borderless progress-red progress--striped">
				<span className="bar" style={{ width: this.state.width }}></span>
			</div>
		</div>
	}
}
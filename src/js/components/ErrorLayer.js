import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

require('../../styles/errorlayer.scss')

class ErrorLayer extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		actions: PropTypes.array,
		error: PropTypes.string,
	}

	render() {
		let actions = this.props.actions.map((action, index) => {
			<li ref={index}><Link to={action.route}>{action.text}</Link></li>
		});

		return <div className="error-layer">
			<div className="error-box">
				<p className="error-message">{this.props.error}</p>
				<ul className="error-actions">
					{actions}
				</ul>
			</div>

		</div>
	}
}

export default ErrorLayer
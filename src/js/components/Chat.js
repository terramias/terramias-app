import React from 'react'
import PropTypes from 'prop-types'

require('../../styles/chat.scss')

export default class Chat extends React.Component {
	/**
	* Define our prop types
	**/
	static propTypes = {
		character: PropTypes.object,
		settings: PropTypes.object,
	}

	constructor(props) {
		super(props)
		this.openChat = this.openChat.bind(this)
		this.closeChat = this.closeChat.bind(this)
		this.closeChannel = this.closeChannel.bind(this)
	}

	openChat() {

	}

	closeChat() {

	}

	showChannel() {

	}

	closeChannel() {

	}

	render() {
		const currentChannel = {
			name: 'Lobby',
			users: [],
			log: [],
		}
		const channels = {
			public: [
				{
					id: 1, name: 'Lobby'
				},
				{
					id: 2, name: 'Markt'
				},
				{
					id: 3, name: 'Hilfe'
				},
				{
					id: 4, name: 'Minispiele'
				},
				{
					id: 5, name: 'Kämpfe'
				},
				{
					id: 6, name: 'Gilde'
				},
				{
					id: 7, name: 'Clubs'
				},
				{
					id: 8, name: 'Essen'
				},
				{
					id: 9, name: 'Baden'
				}
			],
			private: [
				{
					id: 123, name: 'AI Gilde'
				},
				{
					id: 145, name: 'Club Landratten'
				},
				{
					id: 124, name: 'Sylena Sturm'
				},
				{
					id: 4, name: 'UweGera'
				}
			]
		}
		return <aside className="chat">
			<header className="chat-header">
				<ul className="chat-active-channels"></ul>
				<button type="button" className="button button--text chat-current-channel">{currentChannel.name}</button>
				<button type="button" className="button button--image chat-open buttons-arrow-down" onClick={this.openChat}></button>
			</header>
			<section className="chat-channels">
				<div className="col1-2">
					<h4>Allgemein</h4>
					<ul className="chat-public-channels">
					{channels.public.map(channel => {
						return <li key={'chat-channel-public-' + channel.name}><button type="button" className="button button--block button--flat chat-channel-button" onClick={this.showChannel.bind(this, channel.id)}>{channel.name}</button></li>
					})}
					</ul>
				</div>
				<div className="col2-2">
					<h4>Privat</h4>
					<ul className="chat-private-channels">
					{channels.private.map(channel => {
						return <li key={'chat-channel-private-' + channel.name}><button type="button" className="button button--block button--flat chat-channel-button" onClick={this.showChannel.bind(this, channel.id)}>{channel.name}</button></li>
					})}
					</ul>
				</div>
			</section>
			<section className="chat-title">
				<div className="chat-channel-name">{currentChannel.name}} ({currentChannel.users.length})</div>
				<button type="button" onClick={this.closeChannel} className="button button--image chat-channel-close buttons-close"></button>
			</section>
			<section className="chat-log">
				{currentChannel.log}
			</section>
			<footer className="chat-footer">
				<button type="button" onClick={this.closeChat} className="button button--image chat-close buttons-arrow-up"></button>
			</footer>
		</aside>
	}
}
import React from 'react'
import PropTypes from 'prop-types'

require('../../styles/minimap.scss')

export default class MiniMap extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		settings: PropTypes.object.isRequired,
		position: PropTypes.object.isRequired,
		minimap: PropTypes.string
	}

	/**
	 * Constructor
	 *
	 * @return {null}
	 */
	constructor(props: Object) {
		super(props)
		this.state = {
			x: 0,
			y: 0,
			scale: 1
		}
	}

	/**
	 * Update the rendered view with the next properties
	 *
	 * @param  {Object} nextProps	Object with the next properties
	 * @return {null}
	 */
	componentWillReceiveProps(nextProps: Object) {
		if(nextProps.position !== this.props.position) {
			const size = this.getContainerSize('.minimap')
			const imageSize = this.getContainerSize('.minimap-image')
			const state = {
				x: Math.max(0, Math.min(size.width - imageSize.width, size.width / 2 - nextProps.position.x / 10 )),
				y: Math.max(0, Math.min(size.height - imageSize.height, size.height / 2 - nextProps.position.y / 10 ))
			}
			this.setState(state)
		}
	}

	/**
	 * Animation function for the requestframe
	 *
	 * @return {null}
	 */
	getContainerSize(selector) {
		return {
			width: document.querySelector(selector).clientWidth,
			height: document.querySelector(selector).clientHeight
		}
	}

	/**
	 * Render the DOM for the map
	 *
	 * @return {null}
	 */
	render() {
		return <div className="minimap">
			<div className="minimap-image" style={{top: this.state.y + 'px', left: this.state.x + 'px'}}>
				{this.props.image}
			</div>
			<div className="minimap-marker"></div>
		</div>
	}
}
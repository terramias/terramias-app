import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Link from 'react-router-dom/Link'
import Popup from './Popup'

import { reactivateUser } from '../actions/user'

require('../../styles/login.scss')

@connect((store) => {
	return {
		user: store.api.user,
	};
})

export default class Reactivate extends Popup {
	constructor() {
		super()
		this.state = {
			email: '',
		}
		this.reactivateUser = this.reactivateUser.bind(this)
	}

  	reactivateUser() {
		this.props.dispatch(reactivateUser(this.state)).then(() => {
			this.props.router.push('/start/activate/' + this.state.email)
		})
		return false
	}

	handleChange(event) {
		this.state[event.target.name] = event.target.value
		return true
	}

	componentWillMount() {
		if(this.props.match.params.email) {
			this.setState({ email: this.props.match.params.email })
		}
	}
	render() {
		return (
			<div className="popup popup--small">
				<header>
					<h2>Aktivierungscode anfordern</h2>
					<button onClick={this.close.bind(this)} className="button--image close"></button>
				</header>
				<section>
					<form>
						<fieldset>
							<p className="formdata formdata--text">
								<label for="reactivate-email">Name/Email</label>
								<input type="text" id="reactivate-email" name="email" defaultValue={this.state.email} onChange={this.handleChange.bind(this)} />
							</p>
							<p className="formdata">
								<button type="button" className="button" onClick={this.reactivateUser}>Aktivierungscode anfordern</button>
							</p>
						</fieldset>
					</form>
				</section>
			</div>
		);
	}
}
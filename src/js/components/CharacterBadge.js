import React from 'react'
import PropTypes from 'prop-types'

import { getFileUrl } from '../actions/file'

export default class CharacterBadge extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		character: PropTypes.object,
		file: PropTypes.object,
		style: PropTypes.object
	}

	render() {
		return <div className="character-badge" style={this.props.style}>
			{this.props.file ? <div style={{ margin: '10px auto 5px auto', border: '1px solid #000', width: '105px', height: '105px' }} onClick={this.onOpenClick}>
				<img src={getFileUrl(this.props.file.id)} style={{ margin: '-1px', width: '105px', height: '105px' }}/>
			</div> : null}
			{this.props.character ? <dl className="datalist datalist--inline datalist--right">
				<dt>Spieltag</dt><dd>{this.props.character.generationDays}</dd>
				<dt>Level</dt><dd>{this.props.character.level}</dd>
			</dl> : null}
		</div>
	}
}
import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router'

require('../../styles/notification.scss')

class Notification extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		notification: PropTypes.object,
	}

	render() {
		return <div className="notification">
			<h3>{this.props.notification.subject}</h3>
			<div>{this.props.notification.content}</div>
		</div>
	}
}

export default Notification
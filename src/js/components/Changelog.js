import React from 'react'
import PropTypes from 'prop-types'
import Popup from './Popup'

require('../../styles/changelog.scss')

export default class Changelog extends Popup {
	/**
	 * Define our prop types
	 **/
	static propTypes = {}

	render() {
		return (
			<div className="popup ">
				<header>
					<h2>Changelog</h2>
					<button onClick={this.close.bind(this)} className="button--image close"></button>
				</header>
				<section>
				</section>
			</div>
		);
	}
}
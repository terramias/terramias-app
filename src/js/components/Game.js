import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Route from 'react-router-dom/Route'
import Redirect from 'react-router/Redirect'
import Switch from 'react-router/Switch'
import screenfull from 'screenfull'

import CharacterBar from './CharacterBar'
import CharacterBonus from './CharacterBonus'
import CharacterResources from './CharacterResources'
import Chat from './Chat'
import GameMap from './GameMap'
import ImageButton from './ImageButton'
import MiniMap from './MiniMap'
import NotificationCenter from './NotificationCenter'
import ProfileButton from './ProfileButton'

import About from './About'
import Attributes from './Attributes'
import Changelog from './Changelog'
import CharacterEditor from './CharacterEditor'
import CharacterProfile from './CharacterProfile'
import Forum from './Forum'
import Friends from './Friends'
import Help from './Help'
import Highscore from './Highscore'
import Imprint from './Imprint'
import Inventory from './Inventory'
import Messaging from './Messaging'
import Minigames from './Minigames'
import Options from './Options'
import Profile from './Profile'
import Quests from './Quests'
import Rules from './Rules'
import Storage from './Storage'
import Support from './Support'
import Welcome from './Welcome'

import { fetchCharacter, fetchCharacters, fetchCharacterNotifications, fetchCharacterMaps, fetchCharacterMapTooltips, fetchCharacterWays, fetchCharacterPortals } from '../actions/character'
import { fetchCharacterMapPosition, updateCharacterMapPosition } from '../actions/character_map_position'
import { activatePortal } from '../actions/portal'
import { finishStartup } from '../actions/app'

require('../../styles/game.scss')

@connect((store) => {
	return {
		actions: store.actions,
		avatar: store.avatar,
		character: store.character,
		character_map_position: store.character_map_position,
		characters: store.characters,
		figure: store.figure,
		land: store.land,
		maps: store.maps,
		notifications: store.notifications,
		portals: store.portals,
		settings: store.setting,
		settings: store.setting,
		tooltips: store.tooltips,
		user: store.user,
		waypoints: store.waypoints,
		ways: store.ways,
	};
})

export default class Game extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		actions: PropTypes.array,
		character: PropTypes.object,
		character_map_position: PropTypes.object,
		characters: PropTypes.array,
		user: PropTypes.object,
		avatars: PropTypes.array,
		avatar: PropTypes.object,
		figure: PropTypes.object,
		dispatch: PropTypes.func,
		land: PropTypes.object,
		maps: PropTypes.array,
		notifications: PropTypes.object,
		portals: PropTypes.array,
		match: PropTypes.object,
		tooltips: PropTypes.array,
		waypoints: PropTypes.array,
		ways: PropTypes.object,
	}

	constructor(props) {
		super(props)
		this.state = {
			fullscreen: false,
			redirect: false,
			enableMinimap: false,
		}

		this.onClickHandler = this.onClickHandler.bind(this)
		this.onUpdateMapHandler = this.onUpdateMapHandler.bind(this)
		this.onPortalHandler = this.onPortalHandler.bind(this)

		this.setupFullscreen()
	}

	setupFullscreen() {
		if (screenfull.enabled) {
			document.addEventListener(screenfull.raw.fullscreenchange, () => {
				document.querySelector('.app').className = 'app' + (screenfull.isFullscreen ? ' app--fullscreen': '')
				this.setState({
					fullscreen: screenfull.isFullscreen
				})
			})
		}

		window.onresize = () => {
		    var maxHeight = window.screen.height,
		        maxWidth = window.screen.width,
		        curHeight = window.innerHeight,
		        curWidth = window.innerWidth;

		    if (maxWidth == curWidth && maxHeight == curHeight) {
				document.querySelector('.app').className = 'app app--fullscreen'
				this.setState({
					fullscreen: true
				})
		    } else {
				if(document.fullscreenElement === undefined || document.fullscreenElement === null) {
					document.querySelector('.app').className = 'app'
					this.setState({
						fullscreen: false
					})
				}
		    }
		}
	}

	/**
	 * Update the characters position on the map
	 *
	 * @param  {object} coordinates 	coordinates of the character on the currently active land-map
	 * @return {promis} promise 		for further handling
	 */
	onUpdateMapHandler({ x, y }) {
		return this.props.dispatch(updateCharacterMapPosition(
			Object.assign(
				{},
				this.props.character_map_position,
				{ x, y }
			)
		))
	}

	/**
	 * Portal handling for the map
	 *
	 * @param  {object} portal 	data of the action
	 * @return {promis} promise for further handling
	 */
	onPortalHandler(portal: Object) {
		console.warn('PORTAL', portal)
		return this.props.dispatch(activatePortal(this.props.character, portal))
	}

	onClickHandler(id, data: Object = {}) {
		if(typeof id !== 'string') {
			id = id.currentTarget.id
		}
		switch(id) {
			case 'fullscreen':
				if (screenfull.enabled) {
					screenfull.request(document.querySelector('.app'));
				}
				break
			case 'minimap':
				this.setState({ enableMinimap: !this.state.enableMinimap })
				break
			default:
				console.warn('Unhandled button-id ' + id)
		}
	}

	componentWillMount() {
		const { dispatch } = this.props
		dispatch(finishStartup())
		dispatch(fetchCharacters()).then(response => {
			if(response.body.data.length > 1) {
				dispatch(fetchCharacter(response.body.data[0]))
				//this.setState({ redirect: '/game/characters' })
			} else if(response.body.data.length === 1) {
				dispatch(fetchCharacter(response.body.data[0]))
				//this.setState({ redirect: '/game/characters' })
			} else {
				this.setState({ redirect: '/game/welcome' })
			}
		})
	}

	componentWillReceiveProps(nextProps) {
		const { dispatch, actions, character, figure, avatar } = nextProps

		if(actions !== this.props.actions) {
			actions.forEach(action => {
				if(action.name === 'reloadMap') {
					this.props.dispatch(fetchCharacterMapPosition(character))
				}
			})
		}
		if(character.id !== 0 && character !== this.props.character) {
			if(figure.id === 0 || avatar.id === 0) {
				this.setState({ redirect: '/game/welcome' })
			} else {
				dispatch(fetchCharacterMapPosition(character))
			}
		}
		if(nextProps.character_map_position.land_id !== this.props.character_map_position.land_id) {
			dispatch(fetchCharacterMaps(character)).then(() => {
				return dispatch(fetchCharacterWays(character))
			}).then(() => {
				return dispatch(fetchCharacterMapTooltips(character))
			}).then(() => {
				return dispatch(fetchCharacterPortals(character))
			}).then(() => {
				return dispatch(fetchCharacterNotifications(character))
			}).catch(error => console.error(error))
		}
	}

	render() {
		const { character, character_map_position, figure, land, maps, notifications, portals, match, settings, tooltips, waypoints, ways } = this.props
		const buttons = {
			storage: '/game/storage',
			message: '/game/messages',
			inventory: '/game/inventory',
			attributes: '/game/attributes',
			friends: '/game/friends',
			minigames: '/game/minigames',
			quests: '/game/quests',
			highscore: '/game/highscore',
			forum: '/game/forum',
			options: '/game/options',
			help: '/game/help',
			logout: '/start/logout'
		}

		let buttonNodes = Object.keys(buttons).map((key, index) => {
			let buttonClass = key + '-button';
			return (
				<li key={key}>
					<ImageButton ref={key} buttonClass={buttonClass} link={buttons[key]} dataId={key} id={key} />
				</li>
			);
		})

		return <section className="game">
			<nav className="menu">
				<ul>
					<li>
						<ProfileButton ref="profile" avatar={this.props.avatar} link="/game/profile" id="profile" />
					</li>
					{buttonNodes}
				</ul>
			</nav>
			<CharacterBar character={character} settings={settings} onClickHandler={this.onClickHandler}/>
			<CharacterBonus character={character} settings={settings}/>
			<CharacterResources character={character} settings={settings}/>
			<NotificationCenter notifications={notifications} settings={settings}/>
			{!screenfull.isFullscreen ? <button type="button" className="button button--image" style={{ position: 'absolute', top: '50px', right: '10px', zIndex: 100, background: 'white', padding: '5px 10px' }} onClick={this.onClickHandler} id="fullscreen">
				<i className="material-icons">fullscreen</i>
			</button> : null }

			<GameMap character={character} position={character_map_position} figure={figure} land={land} maps={maps} portals={portals} tooltips={tooltips} waypoints={waypoints} ways={ways} onPortal={this.onPortalHandler} onUpdate={this.onUpdateMapHandler} onClick={this.onClickHandler} />
			<Chat character={character} settings={settings} />
			{ this.state.redirect !== false ? (<Redirect to={this.state.redirect} />) : null }
			<Route path={`${match.url}/messages/:folder?`} component={Messaging} />
			<Route path={`${match.url}/profile`} component={Profile} />
			<Route path={`${match.url}/storage`} component={Storage} />
			<Route path={`${match.url}/messages`} component={Messaging} />
			<Route path={`${match.url}/inventory`} component={Inventory} />
			<Route path={`${match.url}/attributes`} component={Attributes} />
			<Route path={`${match.url}/friends`} component={Friends} />
			<Route path={`${match.url}/minigames`} component={Minigames} />
			<Route path={`${match.url}/quests`} component={Quests} />
			<Route path={`${match.url}/highscore`} component={Highscore} />
			<Route path={`${match.url}/forum`} component={Forum} />
			<Route path={`${match.url}/options`} component={Options} />
			<Route path={`${match.url}/help`} component={Help} />
			<Route path={`${match.url}/profile`} component={Profile} />
			<Route path={`${match.url}/character/:id`} component={CharacterProfile} />
			<Route path={`${match.url}/character`} component={CharacterEditor} />
			<Route path={`${match.url}/welcome`} component={Welcome} />
			<Route path={`${match.url}/support`} component={Support} />
			<Route path={`${match.url}/about`} component={About} />
			<Route path={`${match.url}/imprint`} component={Imprint} />
			<Route path={`${match.url}/rules`} component={Rules} />
			<Route path={`${match.url}/changelog`} component={Changelog} />
		</section>
	}
}
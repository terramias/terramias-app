import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Dropzone from 'react-dropzone'
import Formsy from 'formsy-react'
import Redirect from 'react-router/Redirect'

import FormInput from './FormInput'
import FormButton from './FormButton'
import FormRadioGroup from './FormRadioGroup'

import { fetchFigures } from '../actions/figure'
import { createAvatar } from '../actions/avatar'
import { uploadFile, getFileUrl } from '../actions/file'
import { fetchCharacter, createCharacter, updateCharacter } from '../actions/character'


require('../../styles/welcome.scss')

@connect((store) => {
	return {
		character: store.character,
		user: store.user,
		avatars: store.avatars,
		avatar: store.avatar,
		figure: store.figure,
		figures: store.figures,
	}
})

export default class Welcome extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		character: PropTypes.object,
		user: PropTypes.object,
		avatars: PropTypes.array,
		avatar: PropTypes.object,
		figure: PropTypes.object,
		figures: PropTypes.array,
		dispatch: PropTypes.func,
	}

	constructor() {
		super()
		this.close = this.close.bind(this)
		this.submit = this.submit.bind(this)
		this.onDrop = this.onDrop.bind(this)
		this.onOpenClick = this.onOpenClick.bind(this)
		this.handleChange = this.handleChange.bind(this)
		this.enableButton = this.enableButton.bind(this)
		this.disableButton = this.disableButton.bind(this)
		this.isLoaded = false
		this.state = {
			canSubmit: false,
			redirect: false,
            isActive: true,
			character: {
				id: 0,
				name: '',
				description: '',
				figure: {
					id: 1
				},
				avatar: {
					id: 0,
					file: {
						id: 0,
					}
				},
				gender: 'w',
			},
			avatarImage: 'img/gui/ui-elements/avatar_profil.png',
		}
	}

	submit() {
		const { dispatch } = this.props
		let action = null
		this.setState({
			isActive: false
		})
		if(this.state.character.id === 0) {
			action = createCharacter(this.state.character)
		} else {
			action = updateCharacter(this.state.character)
		}
		dispatch(action).then(response => {
			return dispatch(fetchCharacter(response.body.data))
		}).then(() => {
			this.setState({
				isActive: false
			})
			this.close()
		}).catch(() => {
			this.setState({
				isActive: true
			})
		})
	}

	enableButton() {
		this.setState({ canSubmit: true })
	}

	disableButton() {
		this.setState({ canSubmit: false })
	}

	close() {
		this.setState({
			redirect: '/game'
		})
	}

	validateForm(formdata = null) {
		if(formdata === null) {
			formdata = this.state.character
		}
		if(
			Number(formdata.avatar.id) > 0
		&&
			formdata.name !== ''
		&&
			formdata.gender !== ''
		&&
			Number(formdata.figure.id) > 0
		&&
			this.props.figures[formdata.figure.id].gender === formdata.gender
		) {
			this.enableButton()
		} else {
			this.disableButton()
		}
	}

	onDrop(files) {
		let fileInfo = {
			preview: files[0].preview,
			name: files[0].name,
			filesize: files[0].size,
			contentType: files[0].type
		}
		this.setState({ avatarImage: fileInfo.preview })

		this.props.dispatch(createAvatar()).then(response => {
			const fileId = response.body.data.relationships.file.data.id
			this.setState({
				character: Object.assign(
					{},
					this.state.character,
					{ avatar: { id: response.body.data.id } }
				)
			})
			return this.props.dispatch(uploadFile(fileId, files[0]))
		}).then(() => {
			this.validateForm()
		}).catch(error => {
			this.validateForm()
			console.info(error)
		})
	}

    onOpenClick() {
      this.dropzone.open();
    }

    componentWillMount() {
		const { dispatch } = this.props

		dispatch(fetchFigures())
    }

	componentWillReceiveProps(nextProps) {
		const { character, avatar, figure } = nextProps
		const state = Object.assign({}, this.state)
		let update = false

		if(state.character.id != character.id) {
			state.character.id = character.id
			update = true
		}
		if(!this.isLoaded) {
			console.info('!', this.state)
			if(this.state.character.name != character.name) {
				state.character.name = character.name
				update = true
			}
			if(this.state.character.gender != character.gender) {
				state.character.gender = character.gender
				update = true
			}
			if(this.state.character.description != character.description) {
				state.character.description = character.description
				update = true
			}
			if(figure && figure.id !== 0 && figure.file && figure.file.id !== 0 && state.character.figure.id != figure.id) {
				state.character.figure.id = figure.id
				state.avatarImage = getFileUrl(figure.file.id)
				update = true
			} else {
				state.character.figure.id = 1
				state.avatarImage = getFileUrl(1)
				update = true
			}
			this.isLoaded = true
		}
		if(avatar && avatar.id !== 0) {
			state.character.avatar.id = avatar.id
			state.character.avatar.file = avatar.file
			update = true
		}

		this.validateForm(state.character)

		if(update) {
			this.setState(state)
		}
	}

	handleChange(event) {
		const formdata = Object.assign(
			{},
			this.state.character
		)

		if(event.id === 'gender') {
			formdata.gender = event.value
			formdata.figure = { id: 0 }
		} else if(event.id === 'figure') {
			formdata.figure = { id: event.value }
		} else if(event.id === 'name') {
			formdata.name = event.value
		} else {
			formdata[event.id] = event.value
		}

		this.validateForm(formdata)

		this.setState({
			character: formdata
		})
		return true
	}

	render() {
		const figures = []
		Object.keys(this.props.figures).forEach(id => {
			const figure = this.props.figures[id]

			if(figure.gender !== this.state.character.gender) {
				return
			}

			figures.push({ value: figure.id, label: (<img src={figure.image} />), style: { display: 'inline-block', margin: '0 10px', padding: '0 20px' }, styleLabel: { display: 'block' }
			})
		})
		const genders = [ { value: 'w', label: 'weiblich', order: 'left' }, { value: 'm', label: 'männlich' }]
		if(this.state.redirect) {
			return <Redirect to={this.state.redirect} />
		}
		return (
			<div className="popup welcome">
				<Dropzone ref={(node) => { this.dropzone = node; }} accept="image/*" style={{}} activeStyle={{}} disableClick={true} onDrop={this.onDrop}>
					<header>
						<h2>{'Herzlich willkommen "' + this.state.character.name  + '"'}</h2>
					</header>
					<Formsy.Form onSubmit={this.submit} className="welcome">
						<section style={{ textAlign: 'center'}}>
							<p>Du bist nur noch einen kleinen Schritt vom Spiel entfernt.<br />Erstelle Dir hier Deinen Charakter für Terramias:</p>
							<div className="character-avatar-box" style={{ textAlign: 'center' }}>
								<div style={{ margin: '10px auto 5px auto', border: '1px solid #000', width: '105px', height: '105px' }} onClick={this.onOpenClick}>
									<img src={this.state.avatarImage} style={{ margin: '-1px', width: '105px', height: '105px' }}/>
								</div>
								<button type="button" className="button button--bordered" onClick={this.onOpenClick}>Avatar hochladen</button>
							</div>
							<FormInput className="formdata formdata--text" value={this.state.character.name} name="name" title={<h4>Wähle Deinen Namen:</h4>} onChange={this.handleChange} />
							<FormRadioGroup name="gender" title={<h4>Wähle Dein Geschlecht:</h4>} value={this.state.character.gender} items={genders} onChange={this.handleChange} />
							<FormRadioGroup name="figure" title={<h4>Wähle Deine Spielfigur aus:</h4>} value={this.state.character.figure.id} items={figures} onChange={this.handleChange} />
						</section>
						<footer style={{textAlign: 'right'}}>
							<FormButton className="button button--bordered button--primary" type="submit" disabled={!this.state.canSubmit || !this.state.isActive} submitted={!this.state.isActive} title="Weiter zum Spiel" />
						</footer>
					</Formsy.Form>
				</Dropzone>
			</div>
		)
	}
}
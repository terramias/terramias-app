import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Link from 'react-router-dom/Link'

import { fetchCharacterProfile, } from '../actions/character'
import CharacterBadge from './CharacterBadge'
import Popup from './Popup'

require('../../styles/characterprofile.scss')

@connect((store) => {
	return {
		character_profile: store.character_profile,
	}
})

export default class CharacterProfile extends Popup {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		dispatch: PropTypes.func,
	}
	constructor() {
		super()
		this.close = this.close.bind(this)
	}

	componentWillMount() {
		const { dispatch } = this.props
		dispatch(fetchCharacterProfile(this.props.match.params.id))
	}

	render() {

		if(this.props.character_profile[this.props.match.params.id] === undefined || this.props.character_profile[this.props.match.params.id] === null) {
			return null
		}
		const profile_data = this.props.character_profile[this.props.match.params.id]
		console.log('profile data', profile_data)
		let clubs = []

		if(profile_data.clubs) {
			clubs = profile_data.clubs.map(club => {
				return <li><Link to={'/game/club/' + club.id}>{club.name}</Link></li>
			})
		}
		return (
			<div className="popup character-profile">
				<header>
					<h2 className={profile_data.online ? 'character-online' : 'character-offline'}>
						{profile_data.name}
						<span className="character-land">{profile_data.land ? profile_data.land.name : '-'}</span>
						<span className="character-rang">Rang: {profile_data.rang}</span>
					</h2>
					<button onClick={this.close} type="button" className="button--image close"></button>
				</header>
				<section>
					<CharacterBadge file={profile_data.file} character={profile_data} style={{ float: 'right' }} />
					<dl className="datalist">
						<dt>Anmeldedatum</dt><dd>{profile_data.createdAt.date.split(' ')[0]}</dd>
						<dt>Sitter</dt><dd>{profile_data.sitter ? profile_data.sitter.name : '-'}</dd>
						<dt>Gilde</dt><dd>{profile_data.guild ? profile_data.guild.name : '-'}</dd>
						<dt>Clubs</dt><dd><ul className="list--none list--scrollable">{clubs}</ul></dd>
					</dl>
					<div>
						<div className="label">Über mich</div>
						<div className="textarea">{profile_data.description}</div>
					</div>
				</section>
				<footer>
				</footer>
			</div>
		)
	}
}
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Link from 'react-router-dom/Link'
import Redirect from 'react-router/Redirect'
import Formsy from 'formsy-react'

import { activateUser } from '../actions/user'
import FormInput from './FormInput'
import FormButton from './FormButton'
import Popup from './Popup'

require('../../styles/login.scss')

@connect((store) => {
	return {
		user: store.user,
		error: store.error,
	};
})

export default class Activate extends Popup {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		dispatch: PropTypes.func,
		error: PropTypes.string,
		token: PropTypes.string,
		email: PropTypes.string,
		params: PropTypes.object,
	}

	constructor() {
		super()
		this.state = {
			redirect: false,
			email: '',
			token: '',
			canSubmit: false,
			isActive: true,
		}
		this.submit = this.submit.bind(this)
		this.enableButton = this.enableButton.bind(this)
		this.disableButton = this.disableButton.bind(this)
		this.handleChange = this.handleChange.bind(this)
		this.close = this.close.bind(this)
	}

	submit(values) {
		this.setState({
			isActive: false
		})
		this.props.dispatch(activateUser(values)).then(response => {
			this.setState({
				isActive: false,
				redirect: '/start/login'
			})
		}).catch(error => {
			this.setState({
				isActive: true
			})
			console.log(error)
		})
		return false
	}

	enableButton() {
		this.setState({ canSubmit: true })
	}

	disableButton() {
		this.setState({ canSubmit: false })
	}

	handleChange() {
		if(this.props.error !== '') {
			this.props.dispatch({ type: 'RESET_ERRORS' })
		}
		return true
	}

	componentWillMount() {
		if(this.props.match.params.email && this.props.match.params.email !== 'undefined') {
			this.setState({ email: this.props.match.params.email })
		}
		if(this.props.match.params.token && this.props.match.params.token !== 'undefined') {
			this.setState({ token: this.props.match.params.token })
		}
	}
	render() {
		if(this.state.redirect) {
			return <Redirect to={this.state.redirect} />
		}
		return <div className="popup popup--small activate">
			<header>
				<h2>Account aktivieren</h2>
				<button onClick={this.close} type="button" className="button--image close"></button>
			</header>
			<section>
				<Formsy.Form onSubmit={this.submit} onValid={this.enableButton} onInvalid={this.disableButton} className="activate">
					<fieldset>
						<div className="error error--form" style={{ display: (this.props.error === 'NOT FOUND' ? 'block' : 'none') }}>
							Der Benutzername oder der Aktivierungscode stimmen nicht! Bitte versuche es noch einmal.
						</div>
						<div className="error error--form" style={{ display: (this.props.error === 'BAD REQUEST' ? 'block' : 'none') }}>
							Der Aktivierungscode ist ungültig oder abgelaufen. Bitte fordere <Link to={'/start/reactivate/' + this.state.email}>einen neuen Aktivierungscode an</Link>
						</div>
						<FormInput className="formdata formdata--text" value={this.state.email} name="email" title="Emailadresse" required validations="isEmail" validationError="Bitte gib eine korrekte Emailadresse ein." />
						<FormInput className="formdata formdata--text" value={this.state.token} name="token" title="Aktivierungscode" required />
						<p className="formdata">
							<FormButton className="button button--bordered" type="submit" disabled={!this.state.canSubmit || !this.state.isActive} submitted={!this.state.isActive} title="Account aktivieren" />
						</p>
						<p className="formdata formdata--info">
							Du hast Deinen Aktivierungscode nicht? <Link to={'/start/reactivate' + (this.state.email ? '/' + this.state.email : '')}>Fordere einen neuen Aktivierungscode an.</Link>.
						</p>
						<p className="formdata formdata--info">
							Du hast noch keinen Zugang? <Link to="/start/register">Registriere Dich und leg los</Link>
						</p>
					</fieldset>
				</Formsy.Form>
			</section>
		</div>
	}
}

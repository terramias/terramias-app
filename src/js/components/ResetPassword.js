import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Redirect from 'react-router/Redirect'
import Formsy from 'formsy-react'

import FormInput from './FormInput'
import FormPasswordStrength from './FormPasswordStrength'
import Popup from './Popup'

@connect((store) => {
	return {
		error: store.error,
	}
})
export default class ResetPassword extends Popup {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		dispatch: PropTypes.func,
		error: PropTypes.string,
		password: PropTypes.string,
		email: PropTypes.string,
		params: PropTypes.object,
		token: PropTypes.string
	}
	constructor() {
		super()
		this.state = {
			email: '',
			password: '',
			token: '',
			canSubmit: false,
			validationErrors: {},
			success: false,
			isActive: true,
		}
		this.submit = this.submit.bind(this)
		this.close = this.close.bind(this)
		this.enableButton = this.enableButton.bind(this)
		this.disableButton = this.disableButton.bind(this)
	}

	submit(data) {
		this.setState({
			isActive: false
		})
		fetch('API_URL' + 'API_PATH' + '/user/password/' + data.email + '/' + data.token, {
			method: 'PUT',
			mode: 'cors',
			headers: {
				'Authorization': '',
			},
			body: {
				password: data.password
			}
		}).then(() => {
			this.setState({
				success: true
			})
		}).catch(() => {
			this.setState({
				isActive: true,
				validationErrors: {
					email: 'Zu dieser Email existiert kein Zugang.'
				}
			})
		})
		return false
	}

	enableButton() {
		this.setState({ canSubmit: true })
	}

	disableButton() {
		this.setState({ canSubmit: false })
	}

	componentWillMount() {
		if(this.props.match.params.email && this.props.match.params.email !== 'undefined') {
			this.setState({ email: this.props.match.params.email })
		}
		if(this.props.match.params.token && this.props.match.params.token !== 'undefined') {
			this.setState({ token: this.props.match.params.token })
		}
		if(this.props.error !== '') {
			this.props.dispatch({ type: 'RESET_ERRORS' })
		}
	}

	render() {
		if(this.state.success) {
			return <Redirect to="/start/login" />
		}
		return (
			<div className="popup popup--small">
				<header>
					<h2>Passwort vergessen</h2>
					<button onClick={this.close.bind(this)} className="button--image close"></button>
				</header>
				<section>
					<Formsy.Form onSubmit={this.submit} onValid={this.enableButton} onInvalid={this.disableButton} onChange={this.validateForm} validationErrors={this.state.validationErrors} className="register">
						<fieldset>
							<FormInput className="formdata--text" value={this.state.email} name="email" title="Emailadresse" validations="isEmail" validationError="Bitte gib eine gültige Emailadresse ein" required />
							<FormPasswordStrength className="formdata--text" value={this.state.password} name="password" title="Passwort" type="password" required validationError="Bitte gib ein Passwort ein" />
							<FormInput className="formdata--text" value={this.state.passwordRepeat} name="passwordRepeat" title="Wiederholung" type="password" validations="equalsField:password" required validationError="Das Passwort stimmt nicht mit dem ersten überein" />
							<FormInput className="formdata--text" value={this.state.token} name="token" title="Sicherheitscode" type="text" required validationError="Bitte gib den Sicherheitscode ein" />
							<p className="formdata">
								<button type="submit" disabled={!this.state.canSubmit || !this.state.isActive} className="button button--bordered">Neues Passwort setzen</button>
							</p>
						</fieldset>
					</Formsy.Form>
				</section>
			</div>
		);
	}
}
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Link from 'react-router-dom/Link'
import Formsy from 'formsy-react'
import Fingerprint2 from 'fingerprintjs2'

import { finishStartup } from '../actions/app'

import { createSession } from '../actions/session'
import FormInput from './FormInput'
import FormButton from './FormButton'
import Popup from './Popup'

require('../../styles/login.scss')

@connect((store) => {
	return {
		error: store.error,
		user: store.api.user,
	};
})

export default class Login extends Popup {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		dispatch: PropTypes.func,
		error: PropTypes.string,
		password: PropTypes.string,
		email: PropTypes.string,
		match: PropTypes.object,
		user: PropTypes.object,
	}

	constructor() {
		super()
		this.state = {
			email: '',
			password: '',
			canSubmit: false,
			isActive: true,
		}
		this.submit = this.submit.bind(this)
		this.handleChange = this.handleChange.bind(this)
		this.close = this.close.bind(this)
		this.resetError = this.resetError.bind(this)
		this.enableButton = this.enableButton.bind(this)
		this.disableButton = this.disableButton.bind(this)
	}

	submit(values) {
		const { dispatch } = this.props
		this.setState({
			isActive: false
		})
		new Fingerprint2().get(browser_id => {
			values.browser_id = browser_id
			values.useragent = navigator.userAgent
			dispatch(createSession(values)).then(() => {
				this.setState({
					isActive: false
				})
				//return dispatch(fetchUser())
			}).catch(error => {
				this.setState({
					isActive: true
				})
				console.log(error)
			})
		})
	}

	enableButton() {
		this.setState({
			canSubmit: true
		})
	}

	disableButton() {
		this.setState({
			canSubmit: false
		})
	}

	handleChange() {
		if(this.props.error !== '') {
			this.props.dispatch({ type: 'RESET_ERRORS' })
		}
		return true
	}

	componentWillMount() {
		const { dispatch } = this.props

		this.resetError()
		if(this.props.match.params.email && this.props.match.params.email !== 'undefined') {
			this.setState({ email: this.props.match.params.email })
		}
		dispatch(finishStartup())
	}

	resetError() {
		if(this.props.error !== '') {
			this.props.dispatch({ type: 'RESET_ERRORS' })
		}
	}

	render() {
		return (
			<div className="popup popup--small login">
				<header>
					<h2>Einloggen</h2>
					<button onClick={this.close} type="button" className="button--image close"></button>
				</header>
				<section>





					<Formsy.Form onValidSubmit={this.submit} onValid={this.enableButton} onInvalid={this.disableButton} className="login">
						<fieldset>
							<div className="error error--form" style={{ display: (this.props.error === 'FAILED TO FETCH' ? 'block' : 'none') }}>
								Der Server konnte nicht erreicht werden. Bitte versuche es noch einmal.
							</div>
							<div className="error error--form" style={{ display: (this.props.error === 'NOT FOUND' ? 'block' : 'none') }}>
								Wir haben kein Benutzerkonto mit diesem Namen oder Emailadresse. Falls Du Dich nicht vertippt hast, <Link to="/start/register">registriere Deinen Account und leg los</Link>.
							</div>
							<FormInput className="formdata formdata--text" value={this.props.email} name="email" title="Emailadresse / Name" required />
							<FormInput className="formdata formdata--text" value="" name="password" title="Passwort" type="password" required />
							<p className="formdata">
								<FormButton className="button button--bordered" type="submit" disabled={!this.state.canSubmit || !this.state.isActive} onSubmit={this.submit} submitted={!this.state.isActive} title="Einloggen" />
							</p>
							<p className="formdata formdata--info">
								Du hast Dein Passwort vergessen? <Link to="/start/lostpassword">Erstelle Dir ein neues Passwort</Link>.
							</p>
							<p className="formdata formdata--info">
								Du hast noch keinen Zugang? <Link to="/start/register">Registriere Deinen Account und leg los</Link>
							</p>
						</fieldset>
					</Formsy.Form>
				</section>
			</div>
		);
	}
}
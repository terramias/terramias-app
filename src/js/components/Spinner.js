import React from 'react'
import PropTypes from 'prop-types'

require('../../styles/spinner.scss')

export default class FormButton extends React.Component {
	render() {
		return <div className="spinner">
			<div className="bounce1"></div>
			<div className="bounce2"></div>
			<div className="bounce3"></div>
		</div>
	}
}
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'

import { finishStartup } from '../actions/app'

require('../../styles/startup.scss')

@connect((store) => {
	return {
		setting: store.api.setting,
		app: store.app,
	};
})

export default class Game extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
	}

	constructor(props) {
		super(props)
		this.hideStartup = this.hideStartup.bind(this)
	}

	hideStartup() {
		this.props.dispatch(finishStartup())
	}

	render() {
		return <div className={'startup' + (this.props.app.startup ? ' startup--active' : ' startup--inactive')}>
			<div class="startup-content">
				<button type="button"  onClick={this.hideStartup} >Loading</button>
			</div>
			{this.props.children}
		</div>
	}
}
import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Formsy from 'formsy-react'

import { lostPassword } from '../actions/user'
import FormInput from './FormInput'
import FormButton from './FormButton'
import Popup from './Popup'

require('../../styles/login.scss')

@connect((store) => {
	return {
		user: store.api.user,
	};
})

export default class LostPassword extends Popup {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		user: PropTypes.object,
		error: PropTypes.string,
		dispatch: PropTypes.func,
	}
	constructor() {
		super()
		this.state = {
			email: '',
			canSubmit: false,
			isActive: true,
			success: false,
		}
		this.submit = this.submit.bind(this)
		this.handleChange = this.handleChange.bind(this)
		this.close = this.close.bind(this)
		this.enableButton = this.enableButton.bind(this)
		this.disableButton = this.disableButton.bind(this)
	}

	submit(values) {
		this.setState({
			isActive: false
		})
		this.props.dispatch(lostPassword(values)).then(response => {
			console.log(response)
			this.setState({
				success: true,
				isActive: false
			})
		}).catch(error => {
			this.setState({
				isActive: true
			})
			console.log(error)
		})
	}

	enableButton() {
		this.setState({
			canSubmit: true
		})
	}

	disableButton() {
		this.setState({
			canSubmit: false
		})
	}

	handleChange() {
		if(this.props.error !== '') {
			this.props.dispatch({ type: 'RESET_ERRORS' })
		}
		return true
	}

	componentWillMount() {
		if(this.props.error !== '') {
			this.props.dispatch({ type: 'RESET_ERRORS' })
		}
	}

	render() {
		return <div className="popup popup--small">
			<header style={{ display: (this.state.success ? 'none' : 'block')}}>
				<h2>Passwort vergessen</h2>
				<button onClick={this.close} type="button" className="button--image close"></button>
			</header>
			<section style={{ display: (this.state.success ? 'none' : 'block')}}>
				<p>
					Du hast Dein Passwort vergessen?
					Kein Problem, gib Deine Emailadresse ein und wir schicken Dir per Email einen Link zu, mit dem Du ein neues Passwort setzen kannst.
				</p>
				<Formsy.Form onSubmit={this.submit} onValid={this.enableButton} onInvalid={this.disableButton} className="activate">
					<fieldset>
						<div className="error error--form" style={{ display: (this.props.error === 'FAILED TO FETCH' ? 'block' : 'none') }}>
							Der Server konnte nicht erreicht werden. Bitte versuche es noch einmal.
						</div>
						<FormInput className="formdata formdata--text" value={this.state.email} name="email" title="Emailadresse" validations="isEmail" validationError="Bitte gib eine gültige Emailadresse ein" required />
						<p className="formdata">
							<FormButton className="button button--bordered" type="submit" disabled={!this.state.canSubmit || !this.state.isActive} submitted={!this.state.isActive} title="Passwort anfordern" />
						</p>
					</fieldset>
				</Formsy.Form>
			</section>
			<header style={{ display: (this.state.success ? 'block' : 'none')}}>
				<h2>Bitte überprüfe Deinen Posteingang</h2>
				<button onClick={this.close} type="button" className="button--image close"></button>
			</header>
			<section style={{ display: (this.state.success ? 'block' : 'none')}}>
				<p>
					Wir haben Dir eine Email mit einem Link zugesendet. Wenn Du auf den Link klickst, kannst Du ein neues Passwort für Deinen Account festlegen.
				</p>
			</section>
		</div>
	}
}
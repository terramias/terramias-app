import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import Link from 'react-router-dom/Link'
import { createResource } from 'redux-json-api'
import PasswordStrengthMeter from "./PasswordStrengthMeter"
import Popup from './Popup'

require('../../styles/register.scss')

@connect((store) => {
	return {
		user: store.api.user,
		character: store.api.character,
	};
})

export default class Support extends Popup {
	constructor() {
		super()
		this.state = {
			user: 0,
			character: 0,
			email: '',
			theme: '',
			content: '',
		}
	}

	sendSupportRequest() {
		const support = { user: this.props.user.id, character: this.props.character.id, email: this.state.email, theme: this.state.theme, content: this.state.content };
		this.props.dispatch(createResource(support))
		return false
	}

	handleChange(event) {
		this.state[event.target.name] = event.target.value
		return true
	}

	render() {
		if(this.props.user !== undefined && this.props.user !== null && this.props.user.data[0].id !== 0) {
			return <div></div>;
		}
		return (
			<div className="popup">
				<header>
					<h2>Supportanfrage stellen</h2>
					<button onClick={this.close.bind(this)} className="button--image close"></button>
				</header>
				<section>
					<form onSubmit={this.sendSupportRequest.bind(this)}>
						<fieldset>
							<p className="formdata formdata--text">
								<label for="support-email">Email</label>
								<input type="text" id="support-email" name="email" onChange={this.handleChange.bind(this)} />
							</p>
							<div className="formdata formdata--text formdata--select">
								<label for="support-theme">Worum geht es?</label>
								<select id="support-theme" name="theme" onChange={this.handleChange.bind(this)}>

								</select>
							</div>
							<p className="formdata formdata--text">
								<label for="support-content">Details</label>
								<textarea id="support-content" name="content" onChange={this.handleChange.bind(this)}></textarea>
							</p>
							<p className="formdata formdata--text">
								<button type="submit">Supportanfrage stellen</button>
							</p>
						</fieldset>
					</form>
				</section>
			</div>
		);
	}
}
import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'

require('../../styles/_imagebuttons.scss')

export default class ImageButton extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		buttonClass: PropTypes.string,
		disabled: PropTypes.bool,
		link: PropTypes.string,
		id: PropTypes.string,
		dataId: PropTypes.string,
	}

	render() {
		let buttonClass = 'button button--image ' + this.props.buttonClass;
		return <Link to={this.props.link} data-id={this.props.dataId} disabled={this.props.disabled ? true : false}>
			<span className={buttonClass}></span>
		</Link>
	}
}
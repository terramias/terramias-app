import React from 'react'
import PropTypes from 'prop-types'
import Link from 'react-router-dom/Link'
import Popup from './Popup'

require('../../styles/imprint.scss')

export default class Imprint extends Popup {

	render() {
		return (
			<div className="popup ">
				<header>
					<h2>Impressum</h2>
					<button onClick={this.close.bind(this)} className="button--image close"></button>
				</header>
				<section>
					<h3></h3>
					<h4>Terramias-Team</h4>
					<dl>
						<dt>Service</dt><dd><a href="mailto:service@terramias.de">service@terramias.de</a></dd>
						<dt>Support</dt><dd><Link to="support">Supportformular</Link></dd>
					</dl>
					<h3>Betrieb und technische Umsetzung</h3>
					<h4>Martin Jahn</h4>
					<address>Pupinweg 7<br />64295 Darmstadt<br />Deutschland</address>
					<dl>
						<dt>Email</dt><dd>martin.jahn@terramias.de</dd>
						<dt>Telefon</dt><dd>0176 766 755 70</dd>
					</dl>
				</section>
			</div>
		);
	}
}
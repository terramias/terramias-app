import React from 'react'
import PropTypes from 'prop-types'

require('../../styles/characterresources.scss')

export default class CharacterResources extends React.Component {
	calculateMoneyParts(money) {
		return [
						Math.floor(money / 100),
						Math.floor(money % 100),
						Math.round(100 * (money - Math.floor(money)))
		]
	}
	render() {
		let { character } = this.props
		let [ money1, money2, money3 ] = this.calculateMoneyParts(character.money)

		return (
<aside className="character-resources">
    <ul>
        <li className="character-points" title="Punkte">{character.points}</li>
        <li className="character-pearls" title="Perlen">{character.pearls}</li>
        <li className="character-creative-points" title="Kreativpunkte">{character.creativePoints}</li>
        <li className="character-money" title="Geld">
            {money1}
            <span className="money1"></span>
            {money2}
            <span className="money2"></span>
            {money3}
            <span className="money3"></span>
        </li>
    </ul>
</aside>
		);
	}
}
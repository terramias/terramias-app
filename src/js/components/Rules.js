import React from 'react'
import PropTypes from 'prop-types'
import ScrollArea from 'react-scrollbar'
import Popup from './Popup'

require('../../styles/rules.scss')

export default class Rules extends Popup {
	constructor() {
		super()
		this.close = this.close.bind(this)
	}

	render() {
		return (
			<div className="popup popup--big">
				<header>
					<h2>Terramias-Regeln</h2>
					<button onClick={this.close} type="button" className="button--image close"></button>
				</header>
				<section><ScrollArea speed={0.8} horizontal={false} vertical={true} smoothScrolling={true}>
<h3>Grundsätzliches</h3>
<ul>
	<li>Die Anmeldung und Benutzung von Terramias ist völlig kostenlos. Es entstehen auch im Spiel keinerlei Kosten.</li>
	<li>Es gibt in Terramias keine kaufbaren Items/Gegenstände, welche eine Spiel-„Pimpung“ ermöglichen.</li>
	<li>Pro Person ist nur EIN Account erlaubt. Eine Zuwiderhandlung führt zu einer Löschung des Accounts.</li>
	<li>Multi-(Mehrfach)-Accounts werden vom System erkannt und führen ausnahmslos zur Sperrung!</li>
	<li>Account-Sitting, Urlaubsvertretung und andere Account-Deals sind verboten. Eine dafür ausgesprochene Aktionssperre kann z.B. ein zeitlich begrenztes Markt-, Handelsverbot sein und ist NICHT verhandelbar.</li>
	<li>Ein Handel über eine dritte Person ist dann legal, wenn beide Spieler online sind. Sollte sich Spieler 1 ausloggen um mit Spieler 2 wieder online zukommen und die Gegenstände entgegenzunehmen, ist das ein Verstoß gegen das Account-Sitting-Verbot.</li>
	<li>Mehrere Spieler von einem PC oder Netzwerk werden automatisch mit einer Aktionssperre untereinander versehen und es werden sämtliche Aktivitäten beobachtet.</li>
	<li>Bei Terramias herrscht freie Namenswahl, aber: Sollte jedoch ein Benutzername/Charaktername gegen "gute Sitten" verstoßen, oder sexuell- oder gewaltverherrlichend empfunden werden, werden wir von einer kommentarlosen Löschung des Accounts Gebrauch machen. Über unzulässige Spielernicks entscheiden die Betreiber.</li>
	<li>Mit dem Anmeldevorgang erklärst du dich damit einverstanden, die Netiquette einzuhalten. Vulgäres, Beleidigungen, Obszönitäten, Stalking oder Propaganda jeglicher politischer Ansichten oder (verbaler) Verstöße führen zur sofortigen Accountlöschung.</li>
	<li>Alle gesprochenen Texte, Posts und Nachrichten drücken die Ansichten des Erstellers aus, die Eigentümer Terramias (Entwickler dieses Spiels) können nicht für den Inhalt geposteten Textes und/oder Nachrichten der Spieler verantwortlich gemacht werden.</li>
	<li>Innerhalb des Spiel ist Teamwork erwünscht. Das Verhalten sollte dann entsprechend eurer Aufgabenstellung und/oder Levels gewählt werden.</li>
	<li>Kein Spieler ist gezwungen sich in der Community/Gilde einzubringen. So manche Aufgabe ist aber nur als Gildenmitglied oder im Team erfüllbar.</li>
	<li>Verletzung von Privatsphäre und Hacking von persönlichen Daten anderer Spieler führt zur Löschung des Accounts und zieht eine strafrechtliche Ahndung nach sich.</li>
</ul>
<h3>Terramias finanziell unterstützen</h3>
<p>
	Terramias ist ein teures Hobbyprojekt. Wer uns freiwillig und auf rein privater Ebene unterstützen möchte, darf dies gerne machen. Dies sollten aber höchstens Kleinbeträge bis 5 Euro pro Zuwendung sein. Es entstehen dabei für beide Seiten keine Verpflichtungen.
</p>
<ul>
	<li>Eine freiwillige Unterstützung führt zu keiner automatischen Begünstigung im Spiel. </li>
	<li>Eine Rückerstattung ist ausgeschlossen. </li>
	<li>Ein Umtausch ist ausgeschlossen.</li>
	<li>Die Betreiber behalten sich das Recht vor, sich für eine Unterstützung erkenntlich zu zeigen.</li>
</ul>
<h3>Fehler und Ausfälle</h3>
<ul>
	<li>Das Ausnutzen von Programmfehlern (Bugs) ist verboten. Sollte ein Fehler bemerkt werden muss dieser gemeldet werden, ansonsten kann dies zu einer Accountsperre führen. </li>
	<li>Gegenstände die durch einen Programm- oder Serverfehler verloren gingen, werden NICHT erstattet! </li>
	<li>Terramias garantiert keine permanente Verfügbarkeit des Spiels, da wir abhängig von unserem Hoster sind. </li>
</ul>
<h3>Regeln</h3>
<ul>
	<li>Es gelten immer die Regeln in der jeweils neuesten Fassung. </li>
	<li>Diese Regeln gelten für die gesamte Spielzeit der User, Änderungen sind möglich und gelten mit dem Veröffentlichungstag.</li>
	<li>Bei Beendigung von Terramias, was wir nicht hoffen, können keine Ansprüche gestellt werden. </li>
</ul>
</ScrollArea>
				</section>
				<footer>
					<p>
					Version: 23. Februar 2017. Der Rechtsweg ist ausgeschlossen!
					</p>
				</footer>
			</div>
		);
	}
}
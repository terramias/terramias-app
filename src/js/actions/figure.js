import { readEndpoint } from 'redux-json-api'

export function fetchFigures() {
	return readEndpoint('figure')
}
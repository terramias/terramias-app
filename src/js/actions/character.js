import { createResource, updateResource, readEndpoint } from 'redux-json-api'

export function fetchCharacters() {
	return readEndpoint('character')
}

export function selectCharacter(character) {
	return {
		payload: {
			character
		},
		type: 'SELECT_CHARACTER'
	}
}

export function fetchCharacterProfile(id) {
	return readEndpoint('character/' + id + '/profile')
}

export function updateCharacter(characterData) {
	const entity = {
		attributes: {},
		id: characterData.id,
		relationships: {},
		type: 'character',
	}

	Object.keys(characterData).forEach(key => {
		if(typeof characterData[key] === 'object' && characterData[key] !== null) {
			entity.relationships[key] = {
				type: key,
				id: characterData[key].id
			}
			return
		}
		entity.attributes[key] = characterData[key]
	})
	return updateResource(entity)
}

export function createCharacter(characterData) {
	if(characterData.id && characterData.id == 0) {
		delete characterData.id
	}
	return createResource({
		attributes: {
			name: characterData.name,
			description: characterData.description,
			gender: characterData.gender,
		},
		relationships: {
			figure: {
				data: {
					type: 'figure',
					id: characterData.figure.id
				}
			},
			avatar: {
				data: {
					type: 'avatar',
					id: characterData.avatar.id
				}
			},
		},
		type: 'character'
	})
}

export function fetchCharacter({ id }) {
	return readEndpoint('character/' + id)
}

export function fetchCharacterNotifications({ id }) {
	return readEndpoint('character/' + id + '/notifications')
}

export function fetchCharacterMaps({ id }) {
	return readEndpoint('character/' + id + '/maps')
}

export function fetchCharacterWays({ id }) {
	return readEndpoint('character/' + id + '/ways')
}

export function fetchCharacterPortals({ id }) {
	return readEndpoint('character/' + id + '/portals')
}

export function fetchCharacterMapTooltips({ id }) {
	return readEndpoint('character/' + id + '/tooltips')
}

export function checkCharacterName({ name }) {
	return readEndpoint('character/checkName/' + name)
}
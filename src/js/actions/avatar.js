import { createResource, readEndpoint } from 'redux-json-api'

export function createAvatar(file) {
	return createResource({
		type: 'avatar',
	})
}

export function fetchAvatars() {
	return readEndpoint('avatar')
}
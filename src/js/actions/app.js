export function finishStartup() {
	return {
		type: 'APP',
		payload: {
			startup: false
		}
	}

}
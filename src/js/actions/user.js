import { createResource, readEndpoint } from 'redux-json-api'

export function fetchUser() {
	return readEndpoint('user')
}

export function registerUser({ email, password, rules, name }) {
	return createResource({
		attributes: {
			email,
			password,
			rules,
		},
		relationships: {
			character: {
				data: {
					type: 'character',
					attributes: {
						name
					}
				}
			}
		},
		type: 'user'
	})
}

export function activateUser({ email, token }) {
	return readEndpoint('user/activate/' + email + '/' + token)
}

export function reactivateUser({ email }) {
	return readEndpoint('user/activate/' + email)
}

export function lostPassword({ email }) {
	return readEndpoint('user/password/' + email)
}

export function resetUser() {
	return {
		type: 'RESET'
	}
}
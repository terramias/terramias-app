
export function uploadFile(id, file) {
	const reader = new FileReader()
	reader.readAsBinaryString(file)
	reader.onload = event => {
		return fetch('API_URL' + 'API_PATH' + '/file/' + id, {
			method: 'POST',
			headers: {
				'Authorization': localStorage.getItem('Authorization'),
				'Content-Type': file.type
			},
			mode: 'cors',
			body: event.currentTarget.result
		})
	}
}

export function getFileUrl(id) {
	return 'API_URL' + 'API_PATH' + '/file/' + id + '?authorization=' + localStorage.getItem('Authorization')
}

export function getImageUrl(section, filename) {
	return 'img/' + section + '/' + filename
}
import { createResource, readEndpoint, deleteResource } from 'redux-json-api'

export function fetchSession() {
	return readEndpoint('session')
}

export function createSession({ email, password , browser_id, useragent }) {
	return createResource({
		attributes: {
			email,
			password,
			browser_id,
			useragent,
		},
		type: 'session',
	})
}

export function deleteSession({ id }) {
	return deleteResource({
		type: 'session',
		id
	})
}
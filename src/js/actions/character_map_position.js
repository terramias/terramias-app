import { updateResource, readEndpoint } from 'redux-json-api'

export function fetchCharacterMapPosition({ id }) {
	return readEndpoint('character/' + id + '/map_position')
}

export function updateCharacterMapPosition(character_map_position) {
	const entity = {
		attributes: character_map_position,
		id: character_map_position.id,
		relationships: {},
		type: 'character_map_position',
	}

	return updateResource(entity)
}

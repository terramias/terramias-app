export function setFormData(id, formdata) {
	return {
		payload: {
			id,
			formdata,
		},
		type: 'SET_FORMDATA'
	}
}

export function resetFormData(id) {
	return {
		payload: {
			id,
		},
		type: 'RESET_FORMDATA'
	}
}

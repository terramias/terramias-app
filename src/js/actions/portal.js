import { readEndpoint } from 'redux-json-api'

export function activatePortal(char, portal) {
	return readEndpoint('character/' + char.id + '/portal/' + portal.id + '/action')
}

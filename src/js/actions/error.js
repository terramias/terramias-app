import { createResource, readEndpoint } from 'redux-json-api'

export function resetErrors() {
	return {
		type: 'RESET_ERRORS'
	}
}
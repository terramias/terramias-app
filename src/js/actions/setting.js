import { readEndpoint } from 'redux-json-api'

export function fetchSettings() {
	return readEndpoint('setting')
}

export function fetchStatus() {
	return readEndpoint('status')
}

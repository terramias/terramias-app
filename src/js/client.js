require('raven')
Raven.config('https://f8de32469b344c7799990d1b1ae81e11@sentry.io/115170', { release: '0.1.1'}).install()

import React from 'react'
import PropTypes from 'prop-types'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { IntlProvider } from 'react-intl'
import offlineHandler from 'offline-plugin/runtime'
import BrowserRouter from 'react-router-dom/BrowserRouter'
import Route from 'react-router-dom/Route'

import store from './store.js'
import App from './components/App'

import WebFont from 'webfontloader'

WebFont.load({
	google: {
		families: ['Nunito']
	}
})

offlineHandler.install({
	onUpdating: () => {
		console.log('SW Event:', 'onUpdating');
	},
	onUpdateReady: () => {
		console.log('SW Event:', 'onUpdateReady');
		// Tells to new SW to take control immediately
		offlineHandler.applyUpdate();
	},
	onUpdated: () => {
		console.log('SW Event:', 'onUpdated');
		// Reload the webpage to load into the new version
		window.location.reload();
	},

	onUpdateFailed: () => {
		console.log('SW Event:', 'onUpdateFailed');
	}
})
/*
const polyfillsLoaded = __load_polyfills__()

polyfillsLoaded(() => {
*/
const element = document.createElement('div')
element.setAttribute('id', 'app')
document.body.appendChild(element)
const supportsHistory = 'pushState' in window.history

// this is the default behavior
const getConfirmation = (message, callback) => {
  const allowTransition = window.confirm(message)
  callback(allowTransition)
	}

ReactDOM.render((
	<Provider store={store}>
		<BrowserRouter forceRefresh={!supportsHistory} getUserConfirmation={getConfirmation}>
			<Route path="/" component={App} />
		</BrowserRouter>
	</Provider>
), element)
//})
export default function error(state = '', action) {

	switch (action.type) {
		case 'API_DELETE_FAILED':
		case 'API_CREATE_FAILED':
		case 'API_UPDATE_FAILED':
		case 'API_READ_FAILED':
			console.log(action)
			if(action.error) {
				state = action.payload.message.toUpperCase()
			}
			break
		case 'RESET_ERRORS':
			state = ''
	}
	return state
}
export default function portals(state = {}, action) {

	switch (action.type) {
		case 'API_READ':
			if (action.payload.endpoint.match(/character\/\d+\/portals/)) {
				const data = {}
				action.payload.data.map( portal => {
					data[portal.id] = Object.assign(
						portal.attributes,
						{ id: portal.id }
					)
				})
				state = data
			}
			break
		case 'RESET':
			state = {}
			break
	}
	return state
}
export default function(state = {
	maxUserCharacterAmount: 0,
	maxBonusGames: 0,
	maxBonusFights: 0,
	maxBonusDucks: 0,
}, action) {
	switch (action.type) {
		case 'API_READ':
			if(action.payload.endpoint === 'setting') {
				const settings = {}
				action.payload.data.forEach((data) => {
					settings[data.attributes.key] = data.attributes.value;
				})

				return Object.assign(
					{},
					state,
					settings
				);
			}
		default:
			return state
	}
}
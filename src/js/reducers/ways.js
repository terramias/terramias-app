export default function ways(state = [], action) {

	switch (action.type) {
		case 'API_READ':
			if (action.payload.endpoint.match(/character\/\d+\/ways/)) {
				state = action.payload.data.map( way => {
					return Object.assign(
						{},
						way.attributes,
						{ id: way.id }
					)
				})
			}
			break
		case 'RESET':
			state = []
			break
	}
	return state
}
export default function user(state = {
	id: 0,
	email: '',
	status: 'new',
	characters: [],
	selectedCharacterId: 0,
}, action) {

	switch (action.type) {
		case 'API_CREATE_FAILED':
		case 'API_READ_FAILED':
			if (action.payload.entity && action.payload.entity.type && action.payload.entity.type === 'user') {
				localStorage.removeItem('Authorization')
				return Object.assign(
					{},
					state,
					{ status: 'registered', loggedIn: false },
				)
			}
			if (action.payload.endpoint && action.payload.endpoint.indexOf('user/activate/') === 0) {
				localStorage.removeItem('Authorization')

				return Object.assign(
					{},
					state,
					{ status: 'reactivate', loggedIn: true },
				)
			}
			break
		case 'API_CREATED':
		case 'API_READ':
			if (action.payload && action.payload.type === 'user') {
				const characterId = action.payload.relationships.character.data.length === 0 ? 0 : Number(action.payload.relationships.character.data[0].id);
				return Object.assign(
					{},
					state,
					action.payload.attributes,
					{ id: Number(action.payload.id) },
					{ loggedIn: true },
					{ characters: action.payload.relationships.character.data },
					{ selectedCharacterId:  characterId},
				)
			}
			if (action.payload && action.payload.data && action.payload.data && action.payload.data.type === 'user') {
				const characterId = action.payload.data.relationships.character.data.length === 0 ? 0 : Number(action.payload.data.relationships.character.data[0].id);
				return Object.assign(
					{},
					state,
					action.payload.data.attributes,
					{ id: Number(action.payload.data.id) },
					{ loggedIn: true },
					{ characters: action.payload.data.relationships.character.data },
					{ selectedCharacterId:  characterId},
				)
			}
			break
		case 'RESET':
			return {
				id: 0,
				email: '',
				password: '',
				status: 'new',
				loggedIn: false,
				characters: [],
				selectedCharacterId: 0,
			}
			break
	}
	return state
}
export default function notifications(state = {}, action) {

	switch (action.type) {
		case 'API_READ':
			if (action.payload.data && action.payload.data.length && action.payload.data[0].type === 'notification') {
				state = Object.assign({}, state)

				action.payload.data.forEach(notification => {
					state[notification.id] = Object.assign({}, notification.attributes, { id: notification.id } )
				})
			}
			break
		case 'RESET':
			state = {}
			break
	}
	return state
}
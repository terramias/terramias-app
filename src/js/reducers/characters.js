export default function characters(state = [], action) {

	switch (action.type) {
		case 'API_READ':
			if (action.payload.data && action.payload.endpoint === 'character') {
				action.payload.data.forEach(data => {
					if(data.type !== 'character') {
						return
					}
					let charactersData = {}
					charactersData = data.attributes
					charactersData.id = Number(data.id)
					state = state.concat(charactersData)
				})
			}
			break
		case 'RESET':
			state = []
			break
	}
	return state
}
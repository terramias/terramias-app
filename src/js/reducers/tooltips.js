export default function tooltips(state = [], action) {

	switch (action.type) {
		case 'API_READ':
			if (action.payload.endpoint.match(/character\/\d+\/tooltips/)) {
				state = action.payload.data.map( tooltip => {
					return Object.assign(
						{},
						tooltip.attributes,
						{ id: tooltip.id },
						{ polygon: JSON.parse(tooltip.attributes.polygon) }
					)
				})
			}
			break
		case 'RESET':
			state = []
			break
	}
	return state
}
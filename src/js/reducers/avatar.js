export default function avatar(state = {
	id: 0,
	file: { id: 0 },
	createdAt: 0,
	updatedAt: 0,
}, action) {

	switch (action.type) {
		case 'API_WILL_READ':
			if (action.payload.indexOf('/avatar/') === 0) {
				state = Object.assign(
					{},
					state,
				)
			}
			break
		case 'API_READ_FAILED':
			if (action.payload.endpoint.indexOf('/avatar/') === 0) {
				state = Object.assign(
					{},
					state,
				)
			}
			break
		case 'API_READ':
			if (action.payload.included && action.payload.data.type === 'character') {
				action.payload.included.forEach((data) => {
					if(data.type !== 'avatar') {
						return
					}
					state =  Object.assign(
						{},
						state,
						data.attributes,
						{ file: data.relationships.file.data },
						{ id: Number(data.id) },
					)
				})
			}
			break
		case 'RESET':
			state = {
				id: 0,
				file: { id: 0 },
				createdAt: 0,
				updatedAt: 0,
			}
			break
		default:
			if (action && action.payload && action.payload.type && action.payload.attributes && action.payload.id && action.payload.type === 'avatar') {
				let avatar = Object.assign(
					{},
					action.payload.attributes,
					{ id: Number(action.payload.id) }
				)
				if(action.payload.relationships.file) {
					avatar.file.id = action.payload.relationships.file.data.id
				}

				state =  Object.assign(
					{},
					state,
					avatar
				)
			}
			if (action && action.payload && action.payload.data && action.payload.data.type && action.payload.data.attributes && action.payload.data.id && action.payload.data.type === 'avatar') {
				let avatar = Object.assign(
					{},
					action.payload.data.attributes,
					{ id: Number(action.payload.data.id) }
				)
				if(action.payload.data.relationships.file) {
					avatar.file.id = action.payload.data.relationships.file.data.id
				}

				state =  Object.assign(
					{},
					state,
					avatar
				)
			}
			break
	}
	return state
}
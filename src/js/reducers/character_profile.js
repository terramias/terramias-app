export default function character_profile(state = {}, action) {

	switch (action.type) {
		case 'API_READ':
			if (action.payload.endpoint.match(/character\/\d+\/profile/)) {
				let data = Object.assign(
					{},
					action.payload.data.attributes,
					{ id: Number(action.payload.data.id) }
				)
				action.payload.included.forEach(function (include) {
					data[include.type] = Object.assign(
{},
include.attributes,
{ id: Number(include.id) }
)
				})

				state[data.id] = data
			}
			break
		case 'RESET':
			state = {}
			break
	}
	return state
}
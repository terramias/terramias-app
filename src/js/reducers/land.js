export default function land(state = {
	id: 0,
	name: '',
	background: 0x000000,
	defaultCoords: { x: 0, y: 0 }
}, action) {

	switch (action.type) {
		case 'API_READ':
			if (action.payload.included && action.payload.data.type === 'character_map_position') {
				action.payload.included.forEach((data) => {
					if(data.type !== 'land') {
						return
					}
					state =  Object.assign(
						{},
						state,
						data.attributes,
						{ defaultCoords: { x: data.attributes.defaultCoords.split(':')[0], y: data.attributes.defaultCoords.split(':')[1]} },
						{ id: Number(data.id) },
					)
				})
			}
			break
		case 'RESET':
			state = {
				id: 0,
				name: '',
				background: 0x000000,
				defaultCoords: { x: 0, y: 0 }
			}
			break

	}
	return state
}
export default function maps(state = [], action) {

	switch (action.type) {
		case 'API_READ':
			if (action.payload.endpoint.match(/character\/\d+\/maps/)) {
				state = action.payload.data.map( mapData => {
					return Object.assign(
						{},
						mapData.attributes,
						{ id: mapData.id }
					)
				})
			}
			break
		case 'RESET':
			state = []
			break
	}
	return state
}
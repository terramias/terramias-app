export default function actions(state = [], action) {

	switch (action.type) {
		case 'API_READ':
			if (action.payload.data && action.payload.endpoint.match(/character\/\d+\/portal\/\d+\/action/)) {
				state = []
				action.payload.data.forEach(data => {
					if(data.type !== 'action') {
						return
					}
					let actionData = {}
					actionData = data.attributes
					actionData.id = Number(data.id)
					state.push(actionData)
				})
			}
			break
		case 'RESET':
			state = []
			break
	}
	return state
}
export default function session(state = {
	id: 0,
	authToken: '',
	loggedIn: false,
}, action) {

	switch (action.type) {
		case 'API_CREATE_FAILED':
		case 'API_READ_FAILED':
			if (action.payload.entity && action.payload.entity.type && action.payload.entity.type === 'session') {
				localStorage.removeItem('Authorization')
				state = {
					id: 0,
					authToken: '',
					loggedIn: false,
				}
			}
			break
		case 'API_CREATED':
			if (action.payload && action.payload.type === 'session') {
				return Object.assign(
					{},
					state,
					action.payload.attributes,
					{ id: Number(action.payload.id) },
					{ loggedIn: true },
				)
			}
			if (action.payload && action.payload.data && action.payload.data.type === 'session') {
				return Object.assign(
					{},
					state,
					action.payload.data.attributes,
					{ id: Number(action.payload.data.id) },
					{ loggedIn: true },
				)
			}
			break
		case 'API_READ':
			if (action.payload.data && action.payload.data.type === 'session') {
				return Object.assign(
					{},
					state,
					action.payload.data.attributes,
					{ id: Number(action.payload.data.id) },
					{ loggedIn: true },
				)
			}
			break
		case 'RESET':
			state = {
				id: 0,
				authToken: '',
				loggedIn: false,
			}
			break
	}
	return state
}
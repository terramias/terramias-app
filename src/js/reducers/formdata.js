export default function avatar(state = {}, action) {
	switch (action.type) {
		case 'RESET_FORMDATA':
			if(state[action.payload.id]) {
				delete state[action.payload.id]
				state = Object.assign({}, state)
			}
			break
		case 'SET_FORMDATA':
			state = Object.assign(
				{},
				state,
				{ [action.payload.id]: action.payload.formdata }
			)
			break
		case 'RESET':
			state = {}
	}
	return state
}
export default function(state = {
	id: 0,
	class: '',
	gender: '',
	image: '',
	name: '',
	spriteData: {},
	spriteHeight: 50,
	spriteImage: '',
	spriteWidth: 35,
	status: '',
}, action) {

	switch (action.type) {
		case 'API_READ':
			if (action.payload.included && action.payload.data.type === 'character') {
				action.payload.included.forEach((data) => {
					if(data.type !== 'figure') {
						return
					}
					state =  Object.assign(
						{},
						state,
						data.attributes,
						{ spriteData:  JSON.parse(data.attributes.spriteData) },
						{ id: Number(data.id) },
					)
				})
			}
			break;
		case 'RESET':
			state = {
				id: 0,
				class: '',
				gender: '',
				image: '',
				name: '',
				spriteData: {},
				spriteHeight: 50,
				spriteImage: '',
				spriteWidth: 35,
				status: '',
			}
			break

	}
	return state
}
export default function app(state = { startup: true }, action) {

	switch (action.type) {
		case 'APP':
			state = Object.assign(
				{},
				state,
				action.payload
			)
			break
		case 'RESET':
			state = {}
			break
	}
	return state
}
import { combineReducers } from 'redux'
import { reducer as api } from 'redux-json-api';

import actions from './actions'
import app from './app'
import avatar from './avatar'
import character from './character'
import character_map_position from './character_map_position'
import character_profile from './character_profile'
import characters from './characters'
import error from './error'
import figure from './figure'
import figures from './figures'
import formdata from './formdata'
import land from './land'
import maps from './maps'
import notifications from './notifications'
import portals from './portals'
import session from './session'
import setting from './setting'
import tooltips from './tooltips'
import user from './user'
import waypoints from './waypoints'
import ways from './ways'

export default combineReducers({
	api,
	app,
	actions,
	avatar,
	character,
	character_map_position,
	character_profile,
	characters,
	error,
	figure,
	figures,
	formdata,
	land,
	maps,
	notifications,
	portals,
	session,
	setting,
	tooltips,
	user,
	waypoints,
	ways,
})
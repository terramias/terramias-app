export default function character_map_position(state = {
	id: 0,
	land_id: 0,
	x: 0,
	y: 0
}, action) {

	switch (action.type) {
		case 'API_READ':
			if (action.payload.data && action.payload.data.type === 'character_map_position') {
				state =  Object.assign(
					{},
					state,
					action.payload.data.attributes,
					{ id: Number(action.payload.data.id) },
				)
			}
			break
		case 'RESET':
			state = {
				id: 0,
				land_id: 0,
				x: 0,
				y: 0
			}
			break
	}
	return state
}
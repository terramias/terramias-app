export default function character(state = {
	id: 0,
	name: '',
	description: '',
	lastLogin: 0,
	money: 0,
	pearls: 0,
	points: 0,
	creativePoints: 0,
	generation: 0,
	generationDays: 0,
	bonusFights: 0,
	bonusGames: 0,
	bonusDucks: 0,
	highscorePoints: 0,
	level: 0,
	levelPoints: 0,
	gender: 'w',
	inventoryLevel: 0,
	createdAt: 0,
	updatedAt: 0
}, action) {

	switch (action.type) {
		case 'SELECT_CHARACTER':
			state = Object.assign(
				{},
				state,
				action.payload.character
			)
			break
		case 'API_READ':
			if (action.payload.data && action.payload.data.type === 'character') {
				state =  Object.assign(
					{},
					state,
					action.payload.data.attributes,
					{ bonusDucks: Number(action.payload.data.attributes.bonusDucks) },
					{ bonusFights: Number(action.payload.data.attributes.bonusFights) },
					{ bonusGames: Number(action.payload.data.attributes.bonusGames) },
					{ creativePoints: Number(action.payload.data.attributes.creativePoints) },
					{ generation: Number(action.payload.data.attributes.generation) },
					{ highscorePoints: Number(action.payload.data.attributes.highscorePoints) },
					{ inventoryLevel: Number(action.payload.data.attributes.inventoryLevel) },
					{ level: Number(action.payload.data.attributes.level) },
					{ levelPoints: Number(action.payload.data.attributes.levelPoints) },
					{ money: Number(action.payload.data.attributes.money) },
					{ pearls: Number(action.payload.data.attributes.pearls) },
					{ points: Number(action.payload.data.attributes.points) },
					{ id: Number(action.payload.data.id) }
				)
			}
			break
		case 'RESET':
			state = {
				id: 0,
				name: '',
				description: '',
				lastLogin: 0,
				money: 0,
				pearls: 0,
				points: 0,
				creativePoints: 0,
				generation: 0,
				generationDays: 0,
				bonusFights: 0,
				bonusGames: 0,
				bonusDucks: 0,
				highscorePoints: 0,
				level: 0,
				levelPoints: 0,
				gender: 'w',
				inventoryLevel: 0,
				createdAt: 0,
				updatedAt: 0
			}
			break
	}
	return state
}
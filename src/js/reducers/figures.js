export default function figures(state = {}, action) {

	switch (action.type) {
		case 'API_READ':
			if (action.payload.data && action.payload.data.length && action.payload.data[0].type === 'figure') {
				state = Object.assign({}, state)

				action.payload.data.forEach(figure => {
					state[figure.id] = Object.assign({}, figure.attributes, { image: figure.attributes.image.replace('assets/', '') } , { id: figure.id } )
				})
			}
			break
	}
	return state
}
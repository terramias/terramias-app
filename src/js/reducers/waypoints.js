export default function waypoints(state = {}, action) {

	switch (action.type) {
		case 'API_READ':
			if (action.payload.endpoint.match(/character\/\d+\/ways/)) {
				state = {}
				if(action.payload.included) {
					action.payload.included.forEach( waypoint => {
						state[waypoint.id] = Object.assign(
							{},
							waypoint.attributes,
							{ id: waypoint.id }
						)
					})
				}
			}
			break
		case 'RESET':
			state = {}
			break
	}
	return state
}
import React from 'react'
import PropTypes from 'prop-types'

require('../../styles/landinfo.scss')

export default class LandInfo extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		land: PropTypes.object,
	}

	/**
	 * Constructor
	 *
	 * @return {null}
	 */
	constructor(props: Object) {
		super(props)

		this.state = {
			name: ''
		}
		this.reset = this.reset.bind(this)
	}

	reset() {
		this.setState({
			name: ''
		})
	}

	componentWillReceiveProps(nextProps: Object) {
		if(nextProps.land !== this.props.land && nextProps.land.name !== this.props.land.name) {
			this.setState({
				name: nextProps.land.name
			})
			window.setTimeout(this.reset, 2000)
		}
	}

	shouldComponentUpdate(nextProps: Object, nextState: Object) {
		return this.state.name !== nextState.name
	}

	render() {
		return <div className="landinfo" style={{ top: (this.state.name === '' ? '-50px' : '20px'), display: (this.state.name === '' ? 'none' : 'block') }}>{this.state.name}</div>
	}
}
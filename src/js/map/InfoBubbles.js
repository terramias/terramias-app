import React from 'react'
import PropTypes from 'prop-types'
import SAT from 'sat'

require('../../styles/infobubbles.scss')

export default class InfoBubbles extends React.Component {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		tooltips: PropTypes.array,
		stage: PropTypes.object.isRequired,
		position: PropTypes.object.isRequired,
	}

	/**
	 * Constructor
	 *
	 * @return {null}
	 */
	constructor(props: Object) {
		super(props)

		this.lastTooltipPositionCheck = { x: 0, y: 0 }
		this.state = {
			showTip: null,
			tooltips: [],
			mousePosition: new SAT.Vector(0, 0)
		}
		this.response = new SAT.Response();
	}

	componentWillReceiveProps(nextProps) {
		if(nextProps.tooltips !== this.props.tooltips) {
			let tooltips = nextProps.tooltips.map(data => {
				return Object.assign(
					{},
					data,
					{
						polygon: new SAT.Polygon(
							new SAT.Vector(data.polygon[0][0], data.polygon[0][1]),
							data.polygon[1].map(point => {
								return new SAT.Vector(point[0] - data.polygon[0][0], point[1] - data.polygon[0][1])
							})
						)
					}
				)
			})

			this.setState({
				tooltips
			})
		}

		if(nextProps.position !== this.props.position) {

			let tooltip = {}

			let point = new SAT.Vector(nextProps.position.x, nextProps.position.y)

			this.state.tooltips.forEach(tip => {
				if(SAT.pointInPolygon(point, tip.polygon, this.response)) {
					tooltip = tip
					console.log('1');
				}
			})
			if(this.state.showTip === null) {
				this.setState({
					showTip: tooltip,
					mousePosition: nextProps.position
				})

			} else if(this.state.showTip.id !== tooltip.id) {
				this.setState({
					showTip: tooltip,
					mousePosition: nextProps.position
				})
			} else {
				this.setState({
					mousePosition: nextProps.position
				})
			}
		}
	}

	shouldComponentUpdate(nextProps: Object, nextState: Object) {
		if(nextState.showTip !== this.state.showTip) {
			return true
		}
		return false
	}

	render() {
		if(this.state.showTip === null || this.state.showTip.id === undefined || this.state.showTip.id === null) {
			return null
		}
		return <div className="infobubble" style={{left: this.state.showTip.polygon.pos.x + this.props.stage.x, top: this.state.showTip.polygon.pos.y + this.props.stage.y}}>
				<div className="infobubble-title">{this.state.showTip.title}</div>
				<div className="infobubble-description">{this.state.showTip.content}</div>
			</div>
	}
}
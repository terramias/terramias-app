import React from 'react'
import PropTypes from 'prop-types'

var PIXI = require('pixi.js')

export default class PathEditor extends PIXI.Container {
	/**
	 * Define our prop types
	 **/
	static propTypes = {
		waypoints: PropTypes.object,
		paths: PropTypes.object,
	}

	/**
	 * Constructor
	 *
	 * @return {null}
	 */
	constructor(props: Object) {
		super(props)
		this.addWaypoint = this.addWaypoint.bind(this)
		this.save = this.save.bind(this)
		this.newPath = this.newPath.bind(this)
	}

	addWaypoint() {
		this.state.waypoints
	}

	save() {

	}

	newPath() {

	}
}
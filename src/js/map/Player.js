import { TweenLite } from 'gsap'
import SAT from 'sat'

var PIXI = require('pixi.js')

import AnimatedCharacter from './AnimatedCharacter'

export default class Player extends PIXI.Container {
	constructor(props) {
		super(props)

		/**
		 * Id of the currently running animation
		 * @type {string}
		 */
		this.animation = null

		/**
		 * Placeholder for the internal tweeen-object
		 * @type {object}
		 */
		this.tween = null

		/**
		 * List of targets to move to
		 * @type {array}
		 */
		this.targets = []

		/**
		 * Offset of the figure to be placed onto the base-point
		 * @type {object}
		 */
		this.offset = { x: 0, y: 0}

		/**
		 * Position of the player
		 * @type {object}
		 */
		this.origin = { x: 0, y: 0 }

		/**
		 * Speed-value
		 * @type {number}
		 */
		this.speed = 75

		/**
		 * SpeedFactor-value
		 * @type {number}
		 */
		this.speedFactor = 1

		this.onUpdateTween = this.onUpdateTween.bind(this)
		this.onFinishTween = this.onFinishTween.bind(this)

		this.eventCounter = 0
		this.box = new SAT.Polygon(
			new SAT.Vector(this.x, this.y),
			[
				new SAT.Vector(),
				new SAT.Vector(0, this.height),
				new SAT.Vector(this.width, this.height),
				new SAT.Vector(this.width, 0)
			]
		)
	}

	/**
	 * Get the vector-box of the player-object
	 *
	 * @return SAT.Polygon
	 */
	getBox() {
		return this.box
	}

	/**
	 * Set the character of the player
	 *
	 * @param {object} character-data of the player
	 *
	 * @return null
	 */
	setPosition({ x, y }) {
		console.warn('PLAYER POSITION', x, y)
		this.position.x = Math.round(x)
		this.position.y = Math.round(y)
	}

	/**
	 * Set the figure of the player
	 *
	 * @param {object} figure-data of the player
	 *
	 * @return null
	 */
	setFigure(figure) {
		if(figure.spriteImage === '') {
			return
		}
		if(!this.animation) {
			this.animation = new AnimatedCharacter()
			this.addChild(this.animation)
		}
		this.offset = {
			x: figure.spriteWidth * 0.5,
			y: figure.spriteHeight - 7,
		}
		this.animation.setFigure(figure)
		this.animation.setSpeedFactor(this.speedFactor)
		this.animation.position = {
			x: 0 - this.offset.x,
			y: 0 - this.offset.y,
		}
	}

	/**
	 * Change the speed-factor of the character
	 *
	 * @param {number} speedFactor speed-factor
	 *
	 * @return null
	 */
	setSpeedFactor(speedFactor) {
		this.speedFactor = (this.speedFactor === null ? 1 : speedFactor)
		if(this.animation) {
			this.animation.setSpeedFactor(this.speedFactor)
		}
		if(this.tween) {
			this.tween.timeScale(this.speedFactor).play()
		}
	}

	/**
	 * Move the player to a position
	 *
	 * @param {object} position Target-position of the player
	 *
	 * @return null
	 */
	moveTo({x, y}) {
		if(this.animation === undefined || this.animation === null) {
			return
		}

		this.block = false
		if(this.position.x < x) {
			this.animation.useAnimation('walking_right')
		} else if(this.position.x > x) {
			this.animation.useAnimation('walking_left')
		} else if (this.position.y < y) {
			this.animation.useAnimation('walking_right')
		} else {
			this.animation.useAnimation('walking_left')
		}
		const time = Math.sqrt((this.position.x - x) * (this.position.x - x) + (this.position.y - y) * (this.position.y - y)) / (this.speed * this.speedFactor)

		this.origin = { x: this.position.x, y: this.position.y }
		this.block = false

		this.tween = new TweenLite(this.position, time, { x, y, overwrite: 'all', ease: 'Linear.easeNone', onUpdate: this.onUpdateTween, onComplete: this.onFinishTween, onCompleteParams: ['{self}'] })
		this.tween.timeScale(this.speedFactor).play()
	}

	/**
	 * Add a path with multiple targets
	 *
	 * @param {array} targets targets of the path
	 */
	setTargets(targets: Array) {
		this.targets = targets
		this.moveTo(this.targets.shift())
	}

	onUpdateTween() {
		if(Math.abs(this.origin.x - this.position.x) > 10 || Math.abs(this.origin.y - this.position.y) > 10) {
			this.origin = { x: this.position.x, y: this.position.y }
			this.move()
		}
	}

	onFinishTween(tween) {
		if(this.targets.length > 0) {
			this.moveTo(this.targets.shift())
			this.move()
		} else {
			if(!this.block) {
				tween.pause()
				this.block = true
				this.animation.useAnimation(this.animation.getCurrentAnimation().replace('walking_', 'standing_'))
				this.move(true)
			}
		}
	}

	/**
	 * Stop the character movement and switch to passive animation
	 *
	 * @return null
	 */
	stop() {
		this.block = true
		this.tween.kill()
		if(this.animation.getCurrentAnimation().indexOf('standing_') >= 0) {
			return
		}
		this.animation.useAnimation(this.animation.getCurrentAnimation().replace('walking_', 'standing_'))
	}
}
var PIXI = require('pixi.js')

import AnimatedCharacter from './AnimatedCharacter'

export default class NonPlayerCharacter extends PIXI.Container {
	constructor(props) {
		super(props)
		this.animation = null
	}

	setFigure(figure) {
		if(!this.animation) {
			this.animation = new AnimatedCharacter()
			this.addChild(this.animation)
		}
		this.animation.setFigure(figure)
		this.animation.useAnimation('Default')
	}
}
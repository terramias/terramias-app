import { astar, Graph } from 'javascript-astar'
import line from 'bresenham-line'

export default class PathFinder {
	constructor() {
	}

	getRoute(x1, y1, x2, y2) {
		return []

		x1 = Math.round(x1 / this.blockSize)
		y1 = Math.round(y1 / this.blockSize)
		x2 = Math.round(x2 / this.blockSize)
		y2 = Math.round(y2 / this.blockSize)

		let start = this.graph.grid[x1][y1]
		let end = this.graph.grid[x2][y2]
		let result = []

		if(start === undefined) {
			start = this.findNearestWays({x: x1, y: y1})
			result = result.concat({ x: start.x, y: start.y })
		}

		if(start.weight > 0) {
			start = this.findNearestWays(start)
			result = result.concat({ x: start.x, y: start.y })
		}

		if(end === undefined) {
			end = this.findNearestWays({x: x2, y: y2})
			result = result.concat({ x: end.x, y: end.y })
		}

		if(end.weight > 0) {
			end = this.findNearestWays(end)
			end = this.graph.grid[end.x][end.y];
		}

		result = result.concat(astar.search(this.graph, start, end, { heuristic: astar.heuristics.diagonal }))

		if(end.x !== x2 || end.y !== y2) {
			result = result.concat({ x: x2, y: y2 })
		}

		result = result.map(coordinate => {
			return {
				y: coordinate.y * this.blockSize,
				x: coordinate.x * this.blockSize
			}
		})
		return result
	}

	update( { width, height, ways, blockSize = 10 } ) {
		let i = 0, j = 0

		this.blockSize = blockSize

		width = Math.ceil(width / this.blockSize)
		height = Math.ceil(height / this.blockSize)

		this.grid = []
		for(i = 0; i < width; i++) {
			this.grid[i] = []
			for(j = 0; j < height; j++) {
				this.grid[i][j] = 0
			}
		}

		ways.forEach(way => {
			this.addWay(way)
		})
		this.graph = new Graph(this.grid, { diagonal: true })
	}

	findNearestWays({ x, y }) {
		for(let i = 0; i < 10; i++) {
			for(let j = 0; j < 10; j++) {
				if(
					(this.graph.grid[x - i] && this.graph.grid[x - i][y - j] === 1)
				||
					(this.graph.grid[x + i] && this.graph.grid[x + i][y - j] === 1)
				||
					(this.graph.grid[x - i] && this.graph.grid[x - i][y + j] === 1)
				||
					(this.graph.grid[x + i] && this.graph.grid[x + i][y + j] === 1)
				) {
					return {
						x: x - j,
						y: y -i
					}
				}
			}

		}

		return { x, y }
	}

	/**
	 * Adds a connection between two waypoints
	 *
	 * @param {object} way object of the way-data
	 */
	addWay(way: Object) {
		let x1 = Math.round(way.start.x / this.blockSize)
		let y1 = Math.round(way.start.y / this.blockSize)
		let x2 = Math.round(way.end.x / this.blockSize)
		let y2 = Math.round(way.end.y / this.blockSize)

		if(this.grid[x1] === undefined || this.grid[x1][y1] === undefined) {
			console.error('GRID', x1, y1, this.grid)
			return
		}
		this.grid[x1][y1] = 0

		const coords = line(
			{
				x: x1,
				y: y1
			}, {
				x: x2,
				y: y2
			}
		)
		let point
		for (point of coords) {
			this.grid[point.x - 1][point.y - 1] = 1
			this.grid[point.x][point.y - 1] = 1
			this.grid[point.x + 1][point.y - 1] = 1
			this.grid[point.x - 1][point.y] = 1
			this.grid[point.x][point.y] = 1
			this.grid[point.x + 1][point.y] = 1
			this.grid[point.x - 1][point.y + 1 ] = 1
			this.grid[point.x][point.y + 1 ] = 1
			this.grid[point.x + 1][point.y + 1] = 1
		}
	}
}
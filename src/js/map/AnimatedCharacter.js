var PIXI = require('pixi.js')

export default class AnimatedCharacter extends PIXI.Container {
	constructor(props) {
		super(props)

		this.animations = {}
		this.currentAnimation = ''
		this.speedFactor = 1
	}

	setFigure(figure) {
		if(this.figure !== figure || this.figure.spriteImage !== figure.spriteImage) {
			this.reset()
			this.redraw(figure).then(() => {
				this.useAnimation('standing_right')
			}).catch((error) => {
				console.log(error)
			})
		}
	}

	setSpeedFactor(speedFactor) {
		this.speedFactor = speedFactor
		if(this.animations[this.currentAnimation] && this.animations[this.currentAnimation].animationSpeed) {
			this.animations[this.currentAnimation].animationSpeed = 0.16 * this.speedFactor
		}
	}

	reset() {
		Object.keys(this.animations).forEach(key => {
			this.removeChild(this.animations[key])
			delete this.animations[key]
		})
	}
	redraw(figure) {
		return new Promise((resolve, reject) => {
			if(figure.spriteImage === '') {
				reject('Invalid sprite-image ' + figure.spriteImage)
				return
			}
			this.figure = figure
			PIXI.loader.add('img/' + this.figure.spriteImage).load(() => {
				Object.keys(this.figure.spriteData).forEach(key => {
					this.animations[key] = this.addAnimation(key, this.figure.spriteData[key])
					this.addChild(this.animations[key])
				})
				resolve()
			})
		})
	}

	addAnimation(key, animationData) {
		let frames = animationData.frames.map(frameData => {
			return PIXI.Texture.fromFrame(key + (frameData[0] < 9 ? '0' : '') + (frameData[0] + 1) + '.png')
		})
		const movie = new PIXI.extras.AnimatedSprite(frames)
		movie.animationSpeed = 0.16 * this.speedFactor
		movie.position.x = 0
		movie.position.y = 0
		return movie
	}

	useAnimation(name) {
		Object.keys(this.animations).forEach(key => {
			if(name === key) {
				if(!this.animations[key].visible) {
					this.animations[key].visible = true
					this.currentAnimation = name
				}
				if(!this.animations[key].playing) {
					this.animations[key].play()
				}
			} else {
				if(this.animations[key].visible) {
					this.animations[key].visible = false
				}
				if(this.animations[key].playing) {
					this.animations[key].stop()
				}
			}
		})
		if(this.animations[this.currentAnimation]) {
			this.animations[this.currentAnimation].animationSpeed = 0.16 * this.speedFactor
		}
	}
	getCurrentAnimation() {
		return this.currentAnimation
	}
}
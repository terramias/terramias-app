# Terramias Frontend Code

Der Frontend-Code von Terramias stellt einen Client für die Serveranwendung von Terramias dar. Die Anwendung in diesem Repository ist für größere Bildschirme geeignet und auf Tastatur sowie Mausbedienung ausgelegt.

[![Dependency Status](https://www.versioneye.com/user/projects/58051cc00007f4001260caa5/badge.svg?style=flat-square)](https://www.versioneye.com/user/projects/58051cc00007f4001260caa5) 
[![build status](https://gitlab.com/terramias/terramias-app/badges/dev/build.svg)](https://gitlab.com/terramias/terramias-app/commits/dev)
Tags: #HTML, #JavaScript, #CSS, #ReactJS, #Redux, #SASS

# Vorraussetzungen

* NodeJS >= 6 mit passender NPM-Version


# Setup

Folgende Befehle sind auf einem Linux-System ausgeführt worden und können auf anderen Umgebungen in der Syntax abweichen.

## Auschecken des Codes aus GitLab

```bash
git clone --recursive git@gitlab.com:terramias/terramias-app.git
```

## Abhängigkeiten installieren

```bash
cd terramias-app
npm install
```

## Entwicklungsserver starten

```bash
npm start
```

Unter der folgenden URL http://127.0.0.1:4000/

### IP und oder Port ändern

Soll die Client-URL geändert werden, dann kann die Client-URL über zwei Umgebungsvariablen HOST und PORT bereitgestellt werden:

```bash
HOST=192.168.0.13 PORT=4040 npm start
```

### API-URL ändern

Soll die API-URL geändert werden, dann kann die API-URL über eine Umgebungsvariable API bereitgestellt werden:

```bash
API="https://api.terramias.work:80/api" npm start
```


## Buildscript aufrufen

```bash
npm run build
```


const path = require('path')
const webpack = require('webpack')
const autoprefixer = require('autoprefixer')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CleanWebpackPlugin = require('clean-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
const SpritesmithPlugin = require('webpack-spritesmith')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const StringReplacePlugin = require('string-replace-webpack-plugin')
const HtmlWebpackExternalsPlugin = require('html-webpack-externals-plugin')
const OfflinePlugin = require('offline-plugin')
const ImageMagick = require('gm').subClass({imageMagick: true})
const WebpackAutoInject = require('webpack-auto-inject-version')

const sassLoaders = [
	'css-loader',
	'sass-loader'
]

const modernizrOptions = {
	"minify": true,
	"options": [
		"setClasses"
	],
	"feature-detects": [
		"applicationcache",
		"canvas",
		"canvastext",
		"crypto",
		"eventlistener",
		"fullscreen-api",
		"hashchange",
		"history",
		"indexeddbblob",
		"input",
		"inputtypes",
		"intl",
		"json",
		"notification",
		"pagevisibility-api",
		"requestanimationframe",
		"svg",
		"webgl",
		"websockets",
		"css/calc",
		"css/lastchild",
		"file/filesystem",
		"network/fetch",
		"storage/localstorage",
		"storage/sessionstorage",
		"webgl/extensions",
		"webrtc/getusermedia",
		"workers/webworkers"
	]
}

exports.generateHtml = function () {
	return {
		plugins: [
			new HtmlWebpackExternalsPlugin({
				externals: [
					{
						module: 'raven',
						entry: 'https://cdn.ravenjs.com/3.8.0/raven.min.js',
						global: 'Raven',
						type: 'js'
					}, {
						module: 'grecaptcha',
						entry: 'https://www.google.com/recaptcha/api.js',
						global: 'Recaptcha',
						type: 'js'
					}, {
						module: 'balloon',
						entry: 'https://cdnjs.cloudflare.com/ajax/libs/balloon-css/0.2.4/balloon.min.css',
						type: 'css'
					}
				]
			}),
			new HtmlWebpackPlugin({
				title: 'Terramias',
				inject: 'true',
			}),
			{
				apply: function (compiler) {
					compiler.plugin('compilation', (compilation) => {
						compilation.plugin('html-webpack-plugin-before-html-processing', (htmlPluginData, callback) => {
							// Check if base tag already exists
							const baseTagRegex = /<base.*?>/i;
							const baseTagMatches = htmlPluginData.html.match(baseTagRegex);
							if (true === baseTagMatches) {
								// Replace only href attribute
								const modifiedBaseTag = baseTagMatches[0].replace(/href="\S+"/i, 'href="/"');
								htmlPluginData.html = htmlPluginData.html.replace(baseTagRegex, modifiedBaseTag);
							} else {
								// Otherwise insert it in top of the head
								htmlPluginData.html = htmlPluginData.html.replace(/<head>/i, '$&' + '<base href="/">');
							}
							callback(null, htmlPluginData);
						});
					})
				}
			}
		]
	}
}

exports.live = function () {
    return {
        resolve: {
            alias: {
                'react': 'react-lite',
                'react-dom': 'react-lite'
															}
											}
							}
			}

exports.modernizr = function (paths) {
	return {
		module: {
			rules: [
				{
					test: /modernizr$/,
					loader: 'webpack-modernizr-loader',
					options: modernizrOptions
				}
			]
		},
		resolve: {
			alias: {
				'modernizr': path.resolve(paths.src, '../modernizr.json')
			}
		}

	}
}
exports.offline = function () {
	return {
		plugins: [
			new OfflinePlugin()
		]
	}
}

exports.devServer = function (paths, options) {
	return {
		watchOptions: {
			// Delay the rebuild after the first change
			aggregateTimeout: 300,
			// Poll using interval (in ms, accepts boolean too)
			poll: 1000
		},
		devServer: {
			// Enable history API fallback so HTML5 History API based
			// routing works. This is a good default that will come
			// in handy in more complicated setups.
			historyApiFallback: true,
			// Unlike the cli flag, this doesn't set
			// HotModuleReplacementPlugin!
			hot: true,
			inline: true,

			// Display only errors to reduce the amount of output.
			stats: 'errors-only',
			// Parse host and port from env to allow customization.
			//
			// If you use Vagrant or Cloud9, set
			// host: options.host || '0.0.0.0';
			//
			// 0.0.0.0 is available to all network devices
			// unlike default `localhost`.
			host: options.host, // Defaults to `localhost`
			port: options.port // Defaults to 8080
		},
		plugins: [
			// Enable multi-pass compilation for enhanced performance
			// in larger projects. Good default.
			new webpack.HotModuleReplacementPlugin({
		})
		]
	};
}

exports.setupProductionMode = function () {
	return {
		plugins: [
			new webpack.DefinePlugin({
				'process.env': {
					'NODE_ENV': JSON.stringify('production')
				}
			})
		]
	}
}

exports.setupJsx = function (paths, options) {
	return {
		module: {
			noParse: /node_modules\/pixi.js\/dist/,
			rules: [
				{
					test: /\.json$/,
					include: path.join(paths.vendor, 'pixi.js'),
					loader: 'json-loader',
				},
				{
					test: /\.jsx?$/,
					loader: 'babel-loader',
					exclude: paths.vendor,
					options: {
						presets: ['react', 'es2015', 'stage-0'],
						plugins: ['react-html-attrs', 'transform-class-properties', 'transform-decorators-legacy', 'transform-react-jsx-img-import'],
					}
				},
				{
					test: /\.jsx?$/,
					exclude: paths.vendor,
					loader: StringReplacePlugin.replace({
						replacements: [
							{
								pattern: /API_URL/ig,
								replacement: function () {
									return options.api.url;
								}
							},
							{
								pattern: /API_PATH/ig,
								replacement: function () {
									return options.api.path;
								}
							}
						]
					})
				}
			]
		},
		plugins: [
			new StringReplacePlugin()
		]
	}
}

exports.setupFonts = function () {
	return {
		module: {
			rules: [
				{
					test: /\.(eot|svg|ttf|woff(2)?)(\?v=\d+\.\d+\.\d+)?/,
					loader: 'url-loader',
					options: {
						limit: 5000,
						name: 'img/img-[hash:6].[ext]'
					}
				}
			]
		}
	}
}

exports.hint = function (paths) {
	return {
		rules: [
			{
				test: /\.jsx?$/,
				loader: 'jshint',
				enforce: 'pre',
				options: {
					// define an include so we check just the files we need
					include: paths.app
				}
			}
		],
	}
}
exports.setupCSS = function (paths) {
	return {
		module: {
			rules: [
				{
					test: /\.scss$/,
					loader: [
						'style-loader',
						'css-loader',
						'sass-loader'
					]
				}
			]
		}
	};
}

exports.setupImages = function () {
	return {
		module: {
			rules: [
				{
					test: /\.(svg|png|jpg|jpeg|gif)$/,
					loader: 'file-loader',
					options: {
						name: 'img/[name].[hash:6].[ext]'
					}
				}
			]
		}
	};
}

exports.minify = function () {
	return {
		plugins: [
			new webpack.optimize.DedupePlugin(),
			new webpack.optimize.OccurenceOrderPlugin(),
			new webpack.optimize.UglifyJsPlugin({
				// Don't beautify output (enable for neater output)
				beautify: false,

				// Eliminate comments
				comments: false,

				sourceMap: true,

				// Compression specific options
				compress: {
					warnings: false,

					// Drop `console` statements
					drop_console: true
				},

				// Mangling specific options
				mangle: {
					// Don't mangle $
					except: ['$'],

					// Don't care about IE8
					screw_ie8: true,

					// Don't mangle function names
					keep_fnames: true
				}
			})
		]
	};
}

exports.setFreeVariable = function (key, value) {
	const env = {};
	env[key] = JSON.stringify(value);

	return {
		plugins: [
			new webpack.DefinePlugin(env)
		]
	};
}

exports.extractBundle = function (options) {
	const entry = {};
	entry[options.name] = options.entries;

	return {
		// Define an entry point needed for splitting.
		entry: entry,
		plugins: [
			// Extract bundle and manifest files. Manifest is
			// needed for reliable caching.
			new webpack.optimize.CommonsChunkPlugin({
				names: [options.name, 'manifest']
			})
		]
	};
}

exports.clean = function (paths) {
	return {
		plugins: [
			new CleanWebpackPlugin([paths.build], {
				root: process.cwd()
			})
		]
	};
}

exports.extractCSS = function (paths) {
	return {
        module: {
            rules: [
                {
                    test: /\.scss$/,
                    loader: ExtractTextPlugin.extract({
                        fallback: "style-loader",
                        use: sassLoaders
																							})
																			}
            ]
											},
        plugins: [
            new ExtractTextPlugin('[name].[hash:6].css')
        ]
							};
}

exports.extractCSSOld = function (paths) {
	return {
		module: {
			loaders: [
				{
					test: /\.scss$/,
					loaders: [
						ExtractTextPlugin.extract({
							fallback: 'style-loader',
							use: sassLoaders.join('!')
						})
					]
				}
			]
		},
		plugins: [
			new ExtractTextPlugin('[name].[hash:6].css')
		],
		resolve: {
			extensions: ['', '.js', '.scss'],
			root: [paths.src]
		}
	};
}

exports.generateSprites = function (paths) {
	return {
		plugins: [
			new SpritesmithPlugin({
				src: {
					cwd: path.resolve(paths.img, 'gui', 'buttons'),
					glob: '*.png'
				},
				target: {
					image: path.resolve(paths.img, 'gui', 'buttons.png'),
					css: path.resolve(paths.styles, '_imagebuttons.scss')
				},
				apiOptions: {
					cssImageRef: '../img/buttons.png'
				}
			}),
			new SpritesmithPlugin({
				src: {
					cwd: path.resolve(paths.img, 'gui', 'icons'),
					glob: '*.png'
				},
				target: {
					image: path.resolve(paths.img, 'icons.png'),
					css: path.resolve(paths.styles, '_icons.scss')
				},
				apiOptions: {
					cssImageRef: '../img/icons.png'
				}
			})
		]
	};
}

exports.generateFavicons = function (paths) {
	return {
		plugins: [
			new FaviconsWebpackPlugin({
				logo: path.resolve(paths.img, 'logo.png'),
				prefix: 'img/favicons/',
				persistentCache: true,
				inject: true,
				background: '#ffffff',
				title: 'Terramias',
				online: true,
				icons: {
					android: true,
					appleIcon: true,
					appleStartup: true,
					coast: true,
					favicons: true,
					firefox: true,
					opengraph: true,
					twitter: true,
					yandex: true,
					windows: true
				}
			}),
		]
	}
}

exports.copyImages = function (paths) {
	return {
		context: paths.src,
		plugins: [
			new CopyWebpackPlugin([
				{
					from: paths.img,
					to: paths.build + '/img'
				},
			], {
				copyUnmodified: true
			}),
			new CopyWebpackPlugin([
				{
					from: paths.js + '/brandflow-push-sw.js',
					to: paths.build + '/brandflow-push-sw.js'
				},
			], {
				copyUnmodified: true
			}),
		]
	}
}

exports.injectVersion = function (version) {
	return {
		plugins: [
			new WebpackAutoInject({
		        components: {
        		    AutoIncreaseVersion: false,
            		InjectByTag: true,
            		InjectAsComment: false
													}
									})
		]
	}
}

exports.generateMapSprites = function (paths) {
	console.log('ERRIR')
}